FROM python:3.10.0rc1-alpine3.14 AS hanger
ENV CHROMIUM_VERSION 91.0.4472.164-r0
ENV XVFB_VERSION 1.20
ENV SCREEN_COLOUR_DEPTH 24
ENV SCREEN_HEIGHT 1080
ENV SCREEN_WIDTH 1920
ENV ROBOT_REPORTS_DIR /opt/robotframework/reports
ENV ROBOT_TESTS_DIR /opt/robotframework/tests
ENV ROBOT_WORK_DIR /opt/robotframework/temp
ENV TZ UTC
COPY app/bin/chromedriver.sh /opt/robotframework/bin/chromedriver
COPY app/bin/chromium-browser.sh /opt/robotframework/bin/chromium-browser
COPY app/bin/run-tests.sh /opt/robotframework/bin/
RUN apk update \
  && apk --no-cache --virtual .build-deps add \
    nano \
    cargo \
    rust \
    gcc \
    g++ \
    libffi-dev \
    linux-headers \
    make \
    musl-dev \
    openssl-dev \
    which \
    wget \
  && apk --no-cache add \
    "chromium~$CHROMIUM_VERSION" \
    "chromium-chromedriver~$CHROMIUM_VERSION" \
    xauth \
    tzdata \
    "xvfb-run~$XVFB_VERSION" \
  && mv /usr/lib/chromium/chrome /usr/lib/chromium/chrome-original \
  && ln -sfv /opt/robotframework/bin/chromium-browser /usr/lib/chromium/chrome \
  && mkdir -p /opt/robotframework/drivers/
FROM hanger AS tarmac
ENV PATH=/opt/robotframework/bin:/opt/robotframework/drivers:$PATH
RUN python3 -m venv /opt
COPY requirements.txt .
RUN pip install -r requirements.txt
FROM tarmac AS runway
COPY app/test /opt/robotframework/tests
COPY app/resources /opt/robotframework/resources
RUN mkdir -p ${ROBOT_REPORTS_DIR}
VOLUME ${ROBOT_REPORTS_DIR}
CMD [ "/bin/sh", "/opt/robotframework/bin/run-tests.sh" ]