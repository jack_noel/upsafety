*** Settings ***
Resource    ../../resources/keywords/global_kw.robot
Resource    ../../resources/keywords/login_kw.robot
Resource    ../../resources/keywords/manage_users_roles_kw.robot
Resource    ../../resources/keywords/owner_data_kw.robot

Test Setup    Test Init Headless
#Test Setup    Test Initialization
Test Teardown    Kill Browser

*** Test Cases ***
Create new user role for mobile
	[Documentation]    UPSAFE-26
	...                https://t2systems.atlassian.net/browse/UPSAFE-26
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-26    P1    UserMgtRole
	Admin Logging In
	Navigate To User Management Role Page
	Create New Mobile User Role

Add another permission for Cloud user role
	[Documentation]    UPSAFE-27
	...                https://t2systems.atlassian.net/browse/UPSAFE-27
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-27
    Admin Logging In
	Navigate To User Management Role Page
	Adding A New Permission To The Admin Role
	Asserting New Permission Checkbox Is Displayed Correctly
	Post Test Clean Up For New Permissions Role To The Admin
	Get Screenshot

Add another permission for Mobile user role
	[Documentation]    UPSAFE-28
	...                https://t2systems.atlassian.net/browse/UPSAFE-28
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-28
	Admin Logging In
	Navigate To User Management Role Page
	Click Element    //*[text()='Test Mobile User Do Not Delete']
	Click Element    //*[@id="jqg_role-permission-grid_10863"]
	Click Element    //*[@id="role-permission-save-btn"]
	Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    Issue
	Assert New Permission Checkbox Is Displayed For Mobile User Correctly
	Get Screenshot
	Post Test Clean Up For New Permissions For Mobile User

Change permission for cloud user role
    [Documentation]    UPSAFE-29
    ...                https://t2systems.atlassian.net/browse/UPSAFE-29
    ...                === END DOCUMENTATION ===
    [Tags]             UPSAFE-29
    Admin Logging In
    Navigate To User Management Role Page
    Click Element    //*[text()='Administrator']
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateiMeterLocations
    Press Keys    None    ENTER
    Click Element    //*[text()='ActivateiMeterLocations']//ancestor::tr//following::td[1]//input
    Click Element    //*[@id="role-permission-save-btn"]
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateiMeterLocations
    Press Keys    None    ENTER
    Element Text Should Be    //*[text()='ActivateiMeterLocations']//ancestor::tr//following::td[3]    Yes
    Click Element    //*[text()='ActivateiMeterLocations']//ancestor::tr//following::td[1]//input
    Click Element    //*[@id="role-permission-save-btn"]
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateiMeterLocations
    Press Keys    None    ENTER
    Element Text Should Be    //*[text()='ActivateiMeterLocations']//ancestor::tr//following::td[3]    No

Change permission for mobile user role
	[Documentation]    UPSAFE-30
	...                https://t2systems.atlassian.net/browse/UPSAFE-30
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-30
	Admin Logging In
	Navigate To User Management Role Page
	Click Element    //*[text()='Test Mobile User Do Not Delete']
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    iChalk
    Press Keys    None    ENTER
    Click Element    //*[text()='iChalk']//ancestor::tr//following::td[1]//input
    Click Element    //*[@id="role-permission-save-btn"]
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    iChalk
    Press Keys    None    ENTER
    Element Text Should Be    //*[text()='iChalk']//ancestor::tr//following::td[3]    Yes
    Click Element    //*[text()='iChalk']//ancestor::tr//following::td[1]//input
    Click Element    //*[@id="role-permission-save-btn"]
    Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    iChalk
    Press Keys    None    ENTER
    Element Text Should Be    //*[text()='iChalk']//ancestor::tr//following::td[3]    No

Assign same role to another cloud user
	[Documentation]    UPSAFE-31
	...                https://t2systems.atlassian.net/browse/UPSAFE-31
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-31
	Admin Logging In
	Navigate To User Management Role Page

Assign same role for another mobile user
	[Documentation]    UPSAFE-32
	...                https://t2systems.atlassian.net/browse/UPSAFE-32
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-32
	Admin Logging In
	Navigate To User Management Role Page

Remove cloud user access
	[Documentation]    UPSAFE-33
	...                https://t2systems.atlassian.net/browse/UPSAFE-33
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-33
	Admin Logging In
	Navigate To User Management Role Page

Remove mobile user access
	[Documentation]    UPSAFE-34
	...                https://t2systems.atlassian.net/browse/UPSAFE-34
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-34
	Admin Logging In
	Navigate To User Management Role Page

Delete cloud user role
	[Documentation]    UPSAFE-35
	...                https://t2systems.atlassian.net/browse/UPSAFE-35
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-35
	${generate_random_cloud_role} =    Generate Random String    4    [NUMBERS]
	${test_payment_clerk_deletion} =    Catenate    ${test_new_user_role_name}    ${generate_random_cloud_role}
	Admin Logging In
	Navigate To User Management Role Page
	Click Element    ${AddUserRoleButton}
	Input Text    ${RoleNameElement}   ${test_payment_clerk_deletion}
	Click Element    ${AddRoleSaveButtonElement}
	Go To    ${TEST_MANAGE_USERS_ROLES_URL}
	Scroll Element Into View    //*[text()='${test_payment_clerk_deletion}']
	Wait Until Element Is Visible    //*[text()='${test_payment_clerk_deletion}']    15
	Element Should Be Visible    //*[text()='${test_payment_clerk_deletion}']    5
	Click Element    //*[text()='${test_payment_clerk_deletion}']
	Click Element    //*[@id='remove-role-btn']
	Input Text    //*[@id='popup_prompt']    ${test_payment_clerk_deletion}
	Click Element    //*[@id="popup_ok"]

Delete mobile user role
	[Documentation]    UPSAFE-36
	...                https://t2systems.atlassian.net/browse/UPSAFE-36
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-36
	Admin Logging In
	Navigate To User Management Role Page

Admin Looking For Required Values When Searching
	[Documentation]    UPSAFE-37
	...                https://t2systems.atlassian.net/browse/UPSAFE-37
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-37
	Admin Logging In
	Navigate To Owner Data Page
	Identifying Required Values For Search Feature
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Owner data search by issue date with type ODN
	[Documentation]    UPSAFE-38
	...                https://t2systems.atlassian.net/browse/UPSAFE-38
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-38
	Admin Logging In
	Navigate To Owner Data Page
	Search By Tickets Issued From Date
	Click Element    ${OwnerDataSearchTypeOwnerDataNeeded}
	Click Element    ${OwnerDataSearchButton}
	Click Element    //*[@id="owner-data-grid"]
	Scroll Element Into View    ${LicenseNumberTitleElement}
	Element Should Be Visible    ${LicenseNumberTitleElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Owner data search by issue date with type ODE
	[Documentation]    UPSAFE-39
	...                https://t2systems.atlassian.net/browse/UPSAFE-39
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-39
	Admin Logging In
	Navigate To Owner Data Page
	Search By Tickets Issued From Date
	Assert Results Are Displayed Correctly When Searching By ODE

Owner data search by issue date with type Failed lookups
	[Documentation]    UPSAFE-40
	...                https://t2systems.atlassian.net/browse/UPSAFE-40
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-40
	Admin Logging In
	Navigate To Owner Data Page
	Search By Tickets Issued From Date
	Click Element    ${OwnerDataSearchTypeFailedLookupsValue}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${FlaggedForServiceTestResult}
	Element Should Be Visible    ${FlaggedForServiceTestResult}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Owner data search by issue date with type ALL
	[Documentation]    UPSAFE-41
	...                https://t2systems.atlassian.net/browse/UPSAFE-41
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-41
	Admin Logging In
	Navigate To Owner Data Page
	Search By Tickets Issued From Date
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${LicenseNumberTitleElement}
	Element Should Be Visible    ${LicenseNumberTitleElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service but needs owner data by issue date
	[Documentation]    UPSAFE-42
	...                https://t2systems.atlassian.net/browse/UPSAFE-42
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-42
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${TicketForServiceButNeedsOwnerResultElement}
	Element Should Be Visible    ${TicketForServiceButNeedsOwnerResultElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service and owner data exists by issue date
	[Documentation]    UPSAFE-43
	...                https://t2systems.atlassian.net/browse/UPSAFE-43
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-43
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${TicketForServiceButNeedsOwnerResultElement}
	Element Should Be Visible    ${TicketForServiceButNeedsOwnerResultElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service include any owner search type
	[Documentation]    UPSAFE-45
	...                https://t2systems.atlassian.net/browse/UPSAFE-45

	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-45
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${FlaggedForServiceTestResultForTest45}
	Element Should Be Visible    ${FlaggedForServiceTestResultForTest45}
	Element Should Be Visible    ${SearchTypeAllTestResult}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Highlights required fields when attempting Search by unpaid days
	[Documentation]    UPSAFE-46
	...                https://t2systems.atlassian.net/browse/UPSAFE-46
	...                  === END DOCUMENTATION ===
	[Tags]    UPSAFE-46
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataNeeded}
	Click Element    ${OwnerDataSearchButton}
	Element Should Be Visible    ${UnpaidDaysValueIsRequiredElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Error displays when attempting to enter unpaid days less than one
	[Documentation]    UPSAFE-47
	...                https://t2systems.atlassian.net/browse/UPSAFE-47
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-47
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Click Element    ${UnpaidDaysTextbox}
	Clear Element Text    ${UnpaidDaysTextbox}
	Input Text    ${UnpaidDaysTextbox}   ${test_unpaid_days_zero_value}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${UnpaidDaysErrorMessage}
	Element Should Be Visible    ${UnpaidDaysErrorMessage}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket needs owner info by unpaid days
	[Documentation]    UPSAFE-48
	...                https://t2systems.atlassian.net/browse/UPSAFE-48
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-48
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Wait Until Element Is Visible    ${UnpaidDaysSearchResultsValueTicket}
	Element Should Be Visible    ${UnpaidDaysSearchResultsValueTicket}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service and owner lookup failed
	[Documentation]    UPSAFE-49
	...                https://t2systems.atlassian.net/browse/UPSAFE-49
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-49
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeFailedLookupsValue}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${TestResultForTest49}
	Element Should Be Visible    ${TestResultForTest49}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}