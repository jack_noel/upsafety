*** Settings ***
Library     OperatingSystem
Resource    ../../resources/keywords/owner_data_kw.robot
Resource    ../../resources/keywords/login_kw.robot
Resource    ../../resources/keywords/global_kw.robot

Test Setup    Test Init Headless
#Test Setup    Test Initialization
Test Teardown    Kill Browser

*** Test Cases ***
Searching any ticket has failed owner lookup by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-50
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeFailedLookupsValue}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${FlaggedForServiceTestResult}
	Element Should Be Visible    ${FlaggedForServiceTestResult}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket that includes all search type by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-51
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    ${UnpaidDaysTestAllResult}
	Element Should Be Visible    ${UnpaidDaysTestAllResult}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service and owner data needed by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-52
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Click Element    ${OwnerDataGridElement}
	Scroll Element Into View    //*[@title='${test_unpaid_days_number}']
	Wait Until Element Is Visible    //*[@title='${test_unpaid_days_number}']
	Element Should Be Visible    //*[@title='${test_unpaid_days_number}']
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service and has owner data by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-53
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Click Element    ${OwnerDataGridElement}
	Scroll Element Into View    //*[@title='${test_unpaid_days_number}']
	Element Should Be Visible    //*[@title='${test_unpaid_days_number}']
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service but needs owner data by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-54
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    365
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeFailedLookupsValue}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Element Text Should Be    ${CornerTableElement}    ${no_records_to_view_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Searching any ticket has service with All lookup type by unpaid days
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-55
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    365
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${OwnerDataSearchButton}
	Element Text Should Be    ${CornerTableElement}    ${no_records_to_view_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Highlights required fields when attempting to Export by issue date
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-56
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${ExportFileButton}
	Element Should Be Visible    ${RequiredValueOneElement}
	Element Should Be Visible    ${RequiredValueTwoElement}
	Element Should Be Visible    ${ExportStateRequiredText}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Highlights required fields when attempting to Export by unpaid days
	[Documentation]    *TEST* Admin looking for required status fields For Exporting Unpaid Days
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-57
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Click Element    ${ExportFileButton}
	Element Should Be Visible    ${UnpaidDaysRequiredText}
	Element Should Be Visible    ${ExportStateRequiredText}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Alert No tickets found when attempting to Export Mich
	[Documentation]    *TEST* Alert Thrown, "No Tickets Found" When attempt to export Michigan state plate with issue date
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-58
	Admin Logging In
	Navigate To Owner Data Page
	Search By Tickets Issued From Date
	Click Element    ${ExportStateDropdownMichiganValue}
	Click Element    ${ExportFileButton}
	Current Frame Is Should Be Alert

Alert No tickets found when attempting to Export Penn
	[Documentation]    *TEST* Alert Thrown, "No Tickets Found" When attempt to export Pennsylvania state plate with issue date
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-59
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_penn_export_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_penn_export_to_date}
	Click Element    ${ExportStateDropdownDefaultValue}
	Click Element    ${ExportStateDropdownPennsylvaniaValue}
	Click Element    ${ExportFileButton}
	Current Frame Is Should Be Alert
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Successful Export Michigan state plate file when search by issue date
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-60
	${PATH} =    catenate    SEPARATOR=    ${test_current_dir_export_file_for_MI}    ${test_name_of_exported_file_for_MI}
	Admin Logging In
	Navigate To Owner Data Page
	Maximize Browser Window
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${ExportStateDropdownDefaultValue}
	Click Element    ${ExportStateDropdownMichiganValue}
	Click Element    ${ExportFileButton}
	Sleep    10
	File Should Exist    ${PATH}
	Post Test File Removal For Michigan

Successful Export Pennsylvania state plate file when search by issue date
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-61
	${PATH} =    catenate    SEPARATOR=    ${test_current_dir_export_file_for_Penn}    ${test_name_of_exported_file_for_Penn}
	Admin Logging In
	Navigate To Owner Data Page
	Maximize Browser Window
	Search By Tickets Issued From Date
	Click Element    ${ExportStateDropdownPennsylvaniaValue}
	Click Element    ${ExportFileButton}
	Sleep    10
	File Should Exist    ${PATH}
	Post Test File Removal For Penn


Highlights required fields when attempting to Import plate
	[Documentation]    *TEST* Admin looking for required status fields For Importing a Plate
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-62
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${ImportFileButton}
	Element Should Be Visible    ${ImportStateRequiredValue}
	Element Should Be Visible    ${ChooseFileRequiredValue}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Alert dialog displays when attempting to import invalid file
	[Documentation]    *TEST*
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-63
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${ImportStateDropdownDefaultValue}
	Click Element    ${ImportStateMichiganValue}
	Choose File    ${ImportBrowseFileButton}    ${invalid_file_import_path}
	Click Element    ${ImportFileButton}
	Current Frame Is Should Be Alert
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Successful import Michigan plate file
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-64
	${PATH} =    catenate    SEPARATOR=    ${test_import_file_dir}    ${test_owner_data_import_file}
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${FlaggedForServiceCheckbox}
	Click Element    ${ImportStateDropdownDefaultValue}
	Click Element    ${ImportStateMichiganValue}
	Choose File    ${ImportBrowseFileButton}    ${PATH}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Successful import Penndot file
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-64
	${PATH} =    catenate    SEPARATOR=    ${test_import_file_dir}    ${test_penndot_import_file}
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${ImportStateDropdownDefaultValue}
	Click Element    ${ImportStatePennValue}
	Choose File    ${ImportBrowseFileButton}    ${PATH}
	Click Element    ${ImportFileButton}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Viewing any ticket detail on owner data page
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-66
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Click Element    //*[@id="owner-data-grid"]
	Scroll Element Into View    //*[@title='{test_indiana_test_number}']
	Wait Until Element Is Visible    //*[@title='{test_indiana_test_number}']
	Click Element    //*[@title='{test_indiana_test_number}']
	Current Frame Should Contain    ${ticket_detail}
	Wait Until Element Is Visible    ${TicketToolBarElement}
	Element Should Be Visible    ${TicketToolBarElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Printed friendly any ticket detail on owner data page
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-67
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Wait Until Element Is Visible    //*[@title='{test_indiana_test_number}']
	Click Element    //*[@title='{test_indiana_test_number}']
	Current Frame Should Contain    ${ticket_detail}
	Wait Until Element Is Visible    ${TicketToolBarElement}
	Click Element    ${TicketToolBarElement}
	Switch Window    NEW
	Wait Until Element Is Visible    ${PrintableTicketNumberElement}
	Element Should Be Visible    ${PrintableTicketNumberElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Canceling add note for ticket on owner data page
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-68
	Admin Logging In
	Navigate To Owner Data Page
	Search By Radio Button With One Value
	Wait Until Element Is Visible    //*[@title='{test_indiana_test_number}']
	Click Element    //*[@title='{test_indiana_test_number}']
	Current Frame Should Contain    ${ticket_detail}
	Wait Until Element Is Visible    ${TicketToolBarElement}
	Click Element    ${AddNotesButtonElementWithinTicketDetail}
	Current Frame Should Contain    Notes
	Element Should Be Visible    ${NotesCancelButton}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
	Click Element    ${NotesCancelButton}

Adding note for ticket on owner data page
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-69
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    20
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Scroll Element Into View    //*[@title='${test_owner_ticket_number}']
	Wait Until Element Is Visible    //*[@title='${test_owner_ticket_number}']
	Click Element    //*[@title='${test_owner_ticket_number}']
	Wait Until Element Is Visible    ${AddNotesButtonElementWithinTicketDetail}
	Click Element    ${AddNotesButtonElementWithinTicketDetail}
	Input Text    ${TextNotesElement}    ${testing_message}
	Click Element    ${SaveNotesButtonElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Highlights required fields when attempting to edit owner data detail
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-70
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    20
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Click Element    ${OwnerDataTableElement}
	Scroll Element Into View    ${ViewingTicketDetailElement}
	Click Element    ${RequiredHighlightsTicketEditButtonWithinTable}
	Scroll Element Into View    ${EditOwnerDataSaveButton}
	Click Element    ${EditOwnerDataSaveButton}
	Element Text Should Be    ${OwnerLastNameErrorMessageElement}    ${test_edit_owner_last_name_error_message}
	Element Text Should Be    ${OwnerAddressLineOneErrorMessageElement}    ${test_edit_owner_address_one_error_message}
	Element Text Should Be    ${OwnerCityErrorMessageElement}    ${test_edit_owner_city_error_message}
	Element Text Should Be    ${OwnerStateErrorMessageElement}    ${test_edit_state_error_message}
	Element Text Should Be    ${OwnerZipCodeErrorMessageElement}    ${test_edit_zip_code_error_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Successful editing of owner data detail
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-71
	${random_last_name} =    Generate Random String    6    [LETTERS]
	${random_four_digit} =    Generate Random String    4    [NUMBERS]
	${random_address} =    Catenate    ${random_four_digit}    ${fake_address_street}
	${random_zip_code} =    Generate Random String    6    [NUMBERS]
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    20
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Click Element    ${OwnerDataTableElement}
	Scroll Element Into View    //*[@title='${successful_test_owner_data_ticket_number}']
	Click Element    //*[@title='${successful_test_owner_data_ticket_number}']//following::a[1]
	Input Text    ${OwnerLastNameElement}   ${random_last_name}
	Input Text    ${OwnerAddressLineOneElement}   ${random_address}
	Input Text    ${OwnerCityElement}   ${test_owner_city}
	Click Element    ${OwnerStateDefaultValueElement}
	Click Element    ${OwnerStateIndianaValueElement}
	Input Text    ${OwnerStateZipCodeElement}   ${random_zip_code}
	Click Element    ${EditOwnerDataSaveButton}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Canceling clear owner info
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-74
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataExists}
	Click Element    ${OwnerDataSearchButton}
	Click Element    //*[@title='${cancelling_owner_data_ticket_number}']//following::a[1]
	Scroll Element Into View    ${EditOwnerDataClearOwnerInfoElement}
	Click Element    ${EditOwnerDataClearOwnerInfoElement}
	Current Frame Should Contain    ${please_confirm_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
