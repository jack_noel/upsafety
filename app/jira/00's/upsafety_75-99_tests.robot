*** Settings ***
Resource    ../../resources/keywords/owner_data_kw.robot
Resource    ../../resources/keywords/login_kw.robot
Resource    ../../resources/keywords/global_kw.robot
Resource    ../../resources/keywords/update_citation_status_kw.robot

Test Setup    Test Init Headless
Test Teardown    Kill Browser

*** Test Cases ***
Successful clear owner info on owner data page
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-75
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataExists}
	Click Element    ${OwnerDataSearchButton}
	Click Element    //*[@title='${cancelling_owner_data_ticket_number}']//following::a[1]
	Scroll Element Into View    ${EditOwnerDataClearOwnerInfoElement}
	Click Element    ${EditOwnerDataClearOwnerInfoElement}
	Current Frame Should Contain    ${please_confirm_message}
	Click Element    ${PopupOkButton}
	Reload Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Post Test Edit Owner Info

Canceling Add column view or display on owner data table
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-76
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${OwnerDataColumnsButtonElement}
	Current Frame Should Contain    ${select_columns_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Adding one additional column view on owner data table
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-77
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${OwnerDataColumnsButtonElement}
	Current Frame Should Contain    ${select_columns_message}
	Click Element    ${IdNumberElement}
	Click Element    //*[text()='${ok_text_message}']
	Element Should Be Visible    ${OwnerDataGridAdditionalElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
	Click Element    ${OwnerDataColumnsButtonElement}
	Click Element    ${ColumnChooserOwnerDataGridElement}
	Click Element    //*[text()='${ok_text_message}']
	Element Should Not Be Visible    ${OwnerDataGridAdditionalElement}

Removing column view on owner data table
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-78
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${OwnerDataColumnsButtonElement}
	Current Frame Should Contain    ${select_columns_message}
	Click Element    ${IssueDateOwnerDataTableElement}
	Click Element    //*[text()='${ok_text_message}']
	Element Should Not Be Visible    ${OwnerDataGridIssueDateElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
	Click Element    ${OwnerDataColumnsButtonElement}
	Click Element    //*[@title='${issue_date}']//following::a[1]
	Click Element    //*[text()='${ok_text_message}']
	Element Should Be Visible    ${OwnerDataGridIssueDateElement}

Adding all column view on owner data table
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-79
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${OwnerDataColumnsButtonElement}
	Current Frame Should Contain    ${select_columns_message}
	Click Element    //*[text()='${add_all}']
	Click Element    //*[text()='${ok_text_message}']
	Click Element    ${OwnerDataColumnsButtonElement}
	Element Text Should Be    ${ColumnOwnerDataGridForViewingElement}    ${seventeen_items}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
	Click Element    //*[text()='${remove_all}']
	Click Element    //*[text()='${ok_text_message}']

Cancel export current grid data view to CSV
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-81
	Admin Logging In
	Navigate To Owner Data Page
	Click Element    ${OwnerDataColumnsButtonElement}
	Current Frame Should Contain    ${select_columns_message}
	Scroll Element Into View    ${TicketNumberTitleElement}
	Click Element    ${TicketNumberTitleElement}
	Click Element    //*[text()='${ok_text_message}']
	Click Element    ${GridViewForCsvElement}
	Current Frame Should Contain    ${confirm_message}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
	Click Element    ${PopupCancelElement}

Sorting each column by ascending/descending order
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-83
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Element Should Be Visible    //*[text()="${ascending_order}"]
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Changing page view
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-85
	Admin Logging In
	Navigate To Owner Data Page
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${OwnerDataSearchTypeAllValue}
	Click Element    ${OwnerDataSearchButton}
	Click Element    ${NextPaginationElement}
	Element Should Be Visible    //*[text()="${pagination_viewpoint}"]
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Warning message displays when attempting to search all citations
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-86
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button Without Options
	Confirmation Warning Popup
	Get Screenshot

Alert message displays when click save on action to perform
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-87
	Admin Logging In
	Navigate To Update New Citation Page
	Click Action To Perform Save Button
	Confirmation Alert Popup
	Get Screenshot

Search tickets with valid citation number
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-88
	Admin Logging In
	Navigate To Update New Citation Page
	Populate Ticket Information
	Select Citation Open Status And Searching
	Assert Citation Number Appears For Test 88

Search tickets with invalid citation number
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-89
	Admin Logging In
	Navigate To Update New Citation Page
	Populate Invalid Citation Number
	Select Citation Payment Plan Status And Searching
	Asserting That No Records Are Displayed
	Get Screenshot

Search tickets with valid ticket number
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-90
	Admin Logging In
	Navigate To Update New Citation Page
	Populate Valid Citation Number
	Select Citation Open Status And Searching
	Asserting That Valid Record Is Displayed
	Get Screenshot

Search tickets with invalid ticket number
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-91
	Admin Logging In
	Navigate To Update New Citation Page
	Populate Invalid Ticket Number
	Click Ticket Search Button Without Options
	Asserting That No Records Are Displayed
	Get Screenshot

Search tickets with valid LP# that already close to citation
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-92
	Admin Logging In
	Navigate To Update New Citation Page
	Populating License Plate Number
	Select Citation Open Status And Searching
	Asserting Reset Button Is Present
	Asserting Citation Number Results Are Present
	Get Screenshot

Search tickets with invalid LP number
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-93
	Admin Logging In
	Navigate To Update New Citation Page
	Populating Invalid License Plate Number And Searching
	Asserting That No Records Are Displayed
	Get Screenshot

Search tickets with invalid citation date
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-95
	Admin Logging In
	Navigate To Update New Citation Page
	Selecting Invalid Citation Date And Searching
	Asserting That No Records Are Displayed
	Get Screenshot

Verify manage grid to select the columns to view on the table
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-95
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button Without Options
	Confirmation Warning Popup, Then Click OK
	Clicking Main Content Button
	Selecting Grid Checkbox
	Click The Ok Text
	Asserting Test 95 Data Is Displayed
    Get Screenshot

Search tickets with invalid notice date
	[Documentation]
	...                Test to search tickets with invalid notice date
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-97
	Admin Logging In
	Navigate To Update New Citation Page
	Populating Invalid Notice Date
	Selecting Excused Status And Searching
	Asserting That No Records Are Displayed
	Get Screenshot

Reset search a ticket on update citation status
	[Documentation]
	...                Test to verify reset button in search a ticket
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-98
	Admin Logging In
	Navigate To Update New Citation Page
	Populating Ticket Number Textbox With Test Data 98 And Searching
	Asserting Test 98 Data Is Displayed
	Clicking Reset Button
	Asserting Test Data For 98 Doesn't Contain Ticket Number
	Get Screenshot
