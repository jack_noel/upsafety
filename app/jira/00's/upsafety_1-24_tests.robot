*** Settings ***
# Resource    *** Methods ***
Resource    ../../resources/keywords/global_kw.robot
Resource    ../../resources/keywords/login_kw.robot
Resource    ../../resources/keywords/setup_location_kw.robot
Resource    ../../resources/keywords/manage_users_kw.robot
Resource    ../../resources/keywords/violations_kw.robot
Resource    ../../resources/keywords/enter_paper_ticket_kw.robot
Resource    ../../resources/keywords/import_ticket_data_kw.robot
Resource    ../../resources/keywords/dropdown_list_data_kw.robot
Resource    ../../resources/keywords/manage_users_roles_kw.robot

Test Setup    Test Init Headless
#Test Setup    Test Initialization
Test Teardown    Kill Browser

*** Test Cases ***
Admin Logging In Successfully
	[Documentation]     UPSAFE-1
	...                 https://t2systems.atlassian.net/browse/UPSAFE-1
	...                 === END DOCUMENTATION ===
	[Tags]              UPSAFE-1    P1    Login
	Admin Logging In

Admin Input Incorrect Password
	[Documentation]    UPSAFE-2
	...                https://t2systems.atlassian.net/browse/UPSAFE-2
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-2    P1    Login
	Admin Inputs Incorrect Password

Creating a new location for the mobile device
	[Documentation]    UPSAFE-3
	...                https://t2systems.atlassian.net/browse/UPSAFE-3
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-3    P1    Setup
	Admin Logging In
	Navigate To Location Page
	Click Add Location Button
	Click On State Dropdown
	Click On City Dropdown
	Populate Location Name
	Populate Location Code

New User Creation
	[Documentation]    UPSAFE-4
	...                https://t2systems.atlassian.net/browse/UPSAFE-4
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-4    P1    Users
	Admin Logging In
	Navigate To User Management Page
	Click Add New Users Button
	Input Data Into All Required Fields For Cloud User
	Reload Page
	Wait Until Element Is Visible    ${UserCreationValidationElement}
	Element Should Be Visible    ${UserCreationValidationElement}

Duplicated Login Name Throws Error
	[Documentation]    UPSAFE-5
	...                https://t2systems.atlassian.net/browse/UPSAFE-5
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-5    P1    Users
	Admin Logging In
	Navigate To User Management Page
	Click Add New Users Button
	Input Duplicated Data For Cloud User
	Element Should Be Visible   ${duplicated_login_error_message}

Delete User From Cloud Application
	[Documentation]    UPSAFE-6
	...                https://t2systems.atlassian.net/browse/UPSAFE-6
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-6    P1    Users
	Admin Logging In
	Navigate To User Management Page
	Click Element    ${DeleteButtonElement}
	Element Should Be Visible    ${UserCreationValidationElement}

Create User For Mobile Application
	[Documentation]    UPSAFE-7
	...                https://t2systems.atlassian.net/browse/UPSAFE-7
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-7    P1    Users
	Admin Logging In
	Navigate To User Management Page
	Click Add New Users Button
	Input Data Into All Required Fields For Mobile User Creation
	Reload Page
	Wait Until Element Is Visible    ${MobileUserCreationValidationElement}
	Element Should Be Visible    ${MobileUserCreationValidationElement}
	Delete Test Mobile User For Test Cleanup

Creating A New Violation That Is Not A Meter Violation
	[Documentation]    UPSAFE-8
	...                https://t2systems.atlassian.net/browse/UPSAFE-8
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-8    P1    Violation
	Admin Logging In
	Navigate To Violations Page
	Adding Violation
	Inputting Required Data For New Non Meter Violation

Error Message Is Displayed When Attempting To Create Violation With Same Name
	[Documentation]    UPSAFE-9
	...                https://t2systems.atlassian.net/browse/UPSAFE-9
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-9    P1    Violations
	Admin Logging In
	Navigate To Violations Page
	Adding Violation
	Input Required Data To Throw Duplication Error
	Asserting Duplicated Error Message Is Displayed

Creating A New Violation That Is A Meter Violation
	[Documentation]    UPSAFE-10
	...                https://t2systems.atlassian.net/browse/UPSAFE-10
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-10    P1    Violations
	Admin Logging In
	Navigate To Violations Page
	Adding Violation
	Input Required Data For New Meter Violation

Entering A New Paper Ticket
	[Documentation]    UPSAFE-11
	...                https://t2systems.atlassian.net/browse/UPSAFE-11
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-11    P1    PaperTicket
	Admin Logging In
	Navigate To Paper Ticket Page
	Input Required Data For Paper Ticket Submission

Adding New Vehicle To Mobile Device
	[Documentation]    UPSAFE-15
	...                https://t2systems.atlassian.net/browse/UPSAFE-15
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-15    P1    DropdownListPage
	Admin Logging In
	Navigate To Data Dropdown List Page
	Inputting New Vehicle Information

Adding New Gender To Mobile Device
	[Documentation]    UPSAFE-16
	...                https://t2systems.atlassian.net/browse/UPSAFE-16
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-16    P1    DropdownListPage
	Admin Logging In
	Navigate To Data Dropdown List Page
	Inputting New Sex Information

Adding New Vehicle Type To Mobile Device
	[Documentation]    UPSAFE-17
	...                https://t2systems.atlassian.net/browse/UPSAFE-17
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-17    P1    DropdownListPage
	Admin Logging In
	Navigate To Data Dropdown List Page
	Inputting New Vehicle Type Information

Adding New Vehicle Color To Mobile Device
	[Documentation]    UPSAFE-18
	...                https://t2systems.atlassian.net/browse/UPSAFE-18
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-18    P1    DropdownListPage
	Admin Logging In
	Navigate To Data Dropdown List Page
	Inputting New Color Information

Adding New Notes To Mobile Device
	[Documentation]    UPSAFE-19
	...                https://t2systems.atlassian.net/browse/UPSAFE-19
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-19    P1    DropdownListPage
	Admin Logging In
	Navigate To Data Dropdown List Page
	Inputting New Notes Information

Download Master Ticket Is Successful
	[Documentation]    UPSAFE-20
	...                https://t2systems.atlassian.net/browse/UPSAFE-20
	...                COMMENT :: WORKS JUST NOT HEADLESS, NEED TO CONTINUE TO LOOK INTO THIS.
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-20    P1    ImportTicketData
	Admin Logging In
	Navigate To Import Ticket Data Page
	Click Download Ticket Master Button
	Import Ticket Master Data Cleanup

Download Ticket Import Template Is Successful
	[Documentation]    UPSAFE-21
	...                https://t2systems.atlassian.net/browse/UPSAFE-21
	...                COMMENT :: WORKS JUST NOT HEADLESS, NEED TO CONTINUE TO LOOK INTO THIS.
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-21    P1    ImportTicketData
	Admin Logging In
	Navigate To Import Ticket Data Page
	Click Download Ticket Template Button
	Ticket Import Template Data Cleanup

Error Message Displays When Attempting To Create User Role With Same Role
	[Documentation]    UPSAFE-22
	...                https://t2systems.atlassian.net/browse/UPSAFE-22
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-22    P1    UserMgtRole
	Admin Logging In
	Navigate To User Management Role Page
	Attempting To Add New Role That Already Exists
	Asserting Correct Error Message Is Displayed For User Role Duplication

Create A New User Role For Cloud
	[Documentation]    UPSAFE-23
	...                https://t2systems.atlassian.net/browse/UPSAFE-23
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-23    P1    UserMgtRole
	Admin Logging In
	Navigate To User Management Role Page
	Create New Cloud User Role, And Asserting That New Role Exists

Error Message Thrown When Attempting To Create User Role For Device With Same Role Name
	[Documentation]    UPSAFE-24
	...                https://t2systems.atlassian.net/browse/UPSAFE-24
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-24    P1    UserMgtRole
	Admin Logging In
	Navigate To User Management Role Page
	Attempting To Add New Role That Already Exists
	Asserting Correct Error Message Is Displayed For User Role Duplication

Entering A New Paper Ticket That Requires Payment
	[Documentation]    UPSAFE-25
	...                https://t2systems.atlassian.net/browse/UPSAFE-25
	...                === END DOCUMENTATION ===
	[Tags]             UPSAFE-25    P1    PaperTicket
	Admin Logging In
	Navigate To Paper Ticket Page
	Inputting Not A Warning Ticket Submission
	Ensure New Paper Ticket Is Visible After Creation