*** Settings ***
Resource    ../../resources/keywords/login_kw.robot
Resource    ../../resources/keywords/global_kw.robot
Resource    ../../resources/keywords/update_citation_status_kw.robot
Resource    ../../resources/keywords/ticket_report_kw.robot

Test Setup    Test Init Headless
Test Teardown    Kill Browser

*** Test Cases ***
Canceling manage grid to select columns on the table
	[Documentation]
	...                Test to verify reset button in search a ticket
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-100
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button Without Options
	Confirmation Warning Popup, Then Click OK
	Clicking The Main Content Button
	Selecting The Checkboxs For Test 100
	Get Screenshot
	Asserting Test 100 Element Isn't Visible

View any citation detail on update citation status page
	[Documentation]
	...                Test to verify reset button in search a ticket
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-101
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Click The Citation Element
	Asserting That Test 101 Element Is Visible
	Get Screenshot

Reprint any citation with public access copy on update citation status page
	[Documentation]
	...                Test to reprint any citation with public access copy on the table
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-102
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Click The Citation Element
	Click Ticket Toolbar Button
	Switching To New Window
	Click Print Button
	Get Screenshot

Reprint any citation without public access copy on update citation status page
	[Documentation]
	...                Test to reprint any citation with public access copy on the table
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-103
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Sleep    2
	Click Button For Test 103
	Asserting Frame Will Contain Citation Status
	Get Screenshot
	Clicking Cancel Button Post Test 103

Verify printer friendly in citation detail on update citation status
	[Documentation]
	...                Test to verify printer friendly button in citation detail
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-104
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Click The Citation Element
	Click Ticket Toolbar Button
	Switching To New Window
	Asserting Printer Page Title Is Print Citation Details
	Get Screenshot

Verify edit citation detail on update citation status
	[Documentation]
	...                Test to verify edit citation detail
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-105
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Click The Citation Element
	Clicking The Edit Button
	Scrolling Element Into View
	Asserting Attachment Element Exists
	Get Screenshot

Edit necessary fields in citation detail on update citation status
	[Documentation]
	...                Test to edit necessary fields on citation detail
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-106
	Admin Logging In
	Navigate To Update New Citation Page
	Click Ticket Search Button
	Confirmation Warning Popup, Then Click OK
	Click The Citation Element
	Clicking The Edit Button Using Id
	Clicking Save Citation Detail Button
	Getting And Asserting Text Should Be The Success Message


Canceling edit citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-107
	Admin Logging In
	Navigate To Update New Citation Page
	Populate Citation Number For Test 107
	Select Citation Open Status And Searching
	Click Element    ${CitationNumberWithinGrid}
	Click Element    ${EditButtonElement}
	Press Keys    None    ESC
	Get Screenshot

Add new attachment with valid file types in citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-108
	Admin Logging In
	Navigate To Update New Citation Page
    Click Element    ${CitationStatusDropdownDefaultStatus}
    Click Element    ${CitationStatusDropdownOpenStatus}
    Click Element    ${SearchATicketSearchButton}
    Input Text    ${CitationNumberGridTextElement}    ${citation_number_test_110}
    Click Element    ${CitationNumberGridResultElement}
    Click Element    ${EditButtonElement}
    Scroll Element Into View    ${AddAttachmentElement}
    Click Element    ${AddAttachmentElement}
    Adding Appropriate Sized File Inside Citation Detail
    Click Element    ${AddNewAttachmentAddButtonElement}
    Scroll Element Into View    ${CitationDetailSaveButton}
    Click Element    ${CitationDetailSaveButton}
	Assert File Is Uploaded Correctly
	Post Test Clean Up For 108

Error message displays when attempting to add new attachment name with invalid characters in citation detail on update citation status
	[Tags]    UPSAFE-110
	Admin Logging In
	Navigate To Update New Citation Page
    Click Element    ${CitationStatusDropdownDefaultStatus}
    Click Element    ${CitationStatusDropdownOpenStatus}
    Click Element    ${SearchATicketSearchButton}
    Input Text    ${CitationNumberGridTextElement}    ${citation_number_test_110}
    Click Element    ${CitationNumberGridResultElement}
    Click Element    ${EditButtonElement}
    Scroll Element Into View    ${AddAttachmentElement}
    Click Element    ${AddAttachmentElement}
    Input Text    ${NewAttachmentNameElement}    ${invalid_characters_for_test_110}
    Click Element    ${AddNewAttachmentAddButtonElement}
    Element Should Contain    ${NewAttachmentNameMessageElement}    ${invalid_characters_message_for_test_110}

Error message displays when attempting to add new attachment with greater file size in citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-111
	Admin Logging In
	Navigate To Update New Citation Page
    Click Element    ${CitationStatusDropdownDefaultStatus}
    Click Element    ${CitationStatusDropdownOpenStatus}
    Click Element    ${SearchATicketSearchButton}
    Input Text    ${CitationNumberGridTextElement}    ${citation_number_test_110}
    Click Element    ${CitationNumberGridResultElement}
    Click Element    ${EditButtonElement}
    Scroll Element Into View    ${AddAttachmentElement}
    Click Element    ${AddAttachmentElement}
    Adding Attachments Inside Citation Detail
    Assert Proper Error Message Is Triggered

Download attachment in citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-114
	Admin Logging In
	Navigate To Update New Citation Page
	Click Element    ${CitationStatusDropdownDefaultStatus}
    Click Element    ${CitationStatusDropdownOpenStatus}
    Click Element    ${SearchATicketSearchButton}
    Input Text    ${CitationNumberGridTextElement}    ${citation_number_test_110}
    Click Element    ${CitationNumberGridResultElement}
    Click Element    //*[@id="td-ticket-attachments"]/fieldset/table/tbody/tr[1]/td[5]/button[1]
    File Should Exist    C:/Users/gabys/Downloads/Stag Rockburg Citizen Portal.pdf
    Post Test Clean Up For 114

Verify maximum number of attachments in citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-115
	Admin Logging In
	Navigate To Update New Citation Page
    Click Element    ${CitationStatusDropdownDefaultStatus}
    Click Element    ${CitationStatusDropdownOpenStatus}
    Click Element    ${SearchATicketSearchButton}
    Input Text    ${CitationNumberGridTextElement}    ${citation_number_test_110}
    Click Element    ${CitationNumberGridResultElement}
    Click Element    ${EditButtonElement}
    Scroll Element Into View    ${AddAttachmentElement}
    Click Element    ${AddAttachmentElement}
    Adding Appropriate Sized File Inside Citation Detail
    Click Element    ${AddNewAttachmentAddButtonElement}
    Element Text Should Be    ${MaxAttachmentsElement}    ${max_attachments_message}

Verify citation status in citation detail on update citation status
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-116
	Admin Logging In
	Navigate To Update New Citation Page
	Click Element    ${SearchATicketSearchButton}
	Confirmation Warning Popup, Then Click OK
	Click Element    ${GridCheckBoxElementForTest116a}
	Click Element    ${GridCheckBoxElementForTest116b}
	Click Element    ${ActionToPerformDropdownDefaultStatus}
	Click Element    ${ActionToPerformDropdownGuiltyStatus}
	Click Element    ${ActionToPerformSaveButton}
	Element Text Should Be    //*[text()='P0000001-1']//following::td[2]    Guilty
	Post Test Clean Up For 116

Canceling run ticket reports
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-119
	Admin Logging In
	Navigate To Ticket Reports Page
	Click Element    ${ReportFormatDropdownDefault}
	Click Element    ${ScofflawReportOption}
	Click Element    ${RunReportButton}
	Current Frame Should Contain    Notice
	Click Element    ${CancelReportButtonWithinNoticePopup}
	Wait Until Element Is Visible    ${RunReportButton}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

#TODO: Need some guidance on popups. Need to investigate more
Successfully generating scofflaw reports
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-120
	Admin Logging In
	Navigate To Ticket Reports Page
	Click Element    ${ReportFormatDropdownDefault}
	Click Element    ${ScofflawReportOption}
	Click Element    ${RunReportButton}
	Click Element    ${GenerateReportButtonWithinNoticePopup}
	Wait Until Element Is Visible    ${SnackBarElement}
	Element Should Be Visible    ${SnackBarElement}
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}

Alert invalid email format on ticket reports
	[Documentation]
	...                === END DOCUMENTATION ===
	[Tags]    UPSAFE-121
	Admin Logging In
	Navigate To Ticket Reports Page
	Click Element    ${ReportFormatDropdownDefault}
	Click Element    ${ScofflawReportOption}
	Click Element    ${RunReportButton}
	Input Text    //*[@id="Recipients"]    jack,noel@t2systems,com
	Click Element    ${GenerateReportButtonWithinNoticePopup}
	Confirmation Alert Popup
