*** Settings ***
Resource    ../../resources/keywords/process_tickets_kw.robot
Resource    ../../resources/keywords/global_kw.robot
Resource    ../../resources/keywords/login_kw.robot

*** Test Cases ***

Alert Message For Total Amount Must Be More Than Zero When Pay Partial Ticket
    [Documentation]
    [Tags]    UPSAFE-251    P1    ProcessTicketTests
    Admin Logging In
    Naviagte To Process Tickets Page
    Narrowing Ticket Search
    Changing Amount To Zero
    Element Should Be Visible    ${TotalAmountPaymentErrorMessageElement}

Alert Message when pay partial reason amount on process ticket page
    [Documentation]
    [Tags]    UPSAFE-253    P1    ProcessTicketTests
    Admin Logging In
    Naviagte To Process Tickets Page
    Narrowing Ticket Search
    Changing Amount, Then Selecting A Partial Payment
    Element Should Be Visible    ${PartialPaymentReasonErrorMessageElement}

Alert Message when pay partial amount on process ticket page
    [Documentation]
    [Tags]    UPSAFE-256    P1    ProcessTicketTests
    Admin Logging In
    Naviagte To Process Tickets Page
    Narrowing Ticket Search
    Changing Amount, Then Selecting A Partial Payment Reason
    Element Should Be Visible    ${PartialPaymentStatusErrorMessageElement}