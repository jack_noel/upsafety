#!/bin/sh
xvfb-run \
    --server-args="-screen 0 ${SCREEN_WIDTH}x${SCREEN_HEIGHT}x${SCREEN_COLOUR_DEPTH} -ac" \
    robot --outputDir "${ROBOT_REPORTS_DIR}" opt/robotframework/tests/
