*** Variables ***
#_________________________________________
# Owner Data, Data.
#_________________________________________
${required_values_upon_search_click} =    Value is required.
${test_ticket_issued_from_date} =    01/01/2021
${test_ticket_issue_to_date} =    10/13/2021
${test_unpaid_days_zero_value} =    0

${test_penn_export_from_date} =    01/01/2021
${test_penn_export_to_date} =    01/02/2021

${test_current_dir_export_file_for_MI} =    C:/Users/gabys/Downloads/
${test_current_dir_export_file_for_Penn} =    C:/Users/gabys/Downloads/
${test_name_of_exported_file_for_MI} =    YOURFILENAME10132021.TXT
${test_name_of_exported_file_for_Penn} =    ROCKBUR1.IN

${test_import_file_dir} =    D:/upsafety_cloud/resources/files_for_testing_purposes/
${test_owner_data_import_file} =    Plate Match_Results_01112018.TXT

${test_penndot_import_file} =    Penndot.out

${test_edit_owner_last_name_error_message} =    Owner Last Name is mandatory.
${test_edit_owner_address_one_error_message} =    Address Line1 is mandatory.
${test_edit_owner_city_error_message} =    City is mandatory.
${test_edit_state_error_message} =    This field is required.
${test_edit_zip_code_error_message} =    Zipcode is mandatory.

${fake_address_street} =    Washington Blvd
${test_owner_city} =    Indianapolis

${invalid_file_import_path} =    ../uploads/test_import_ticket_file.xlsx
${ticket_detail} =    Ticket Detail

${test_indiana_test_number} =    IN 5103798176
${test_unpaid_days_number} =    IN 9570536612
${test_owner_ticket_number} =    IN 6334298594
${successful_test_owner_data_ticket_number} =    IN 6334298594
${cancelling_owner_data_ticket_number} =    IN 0984109064
${no_records_to_view_message} =    No records to view
${testing_message} =    testing
${please_confirm_message} =    Please Confirm
${confirm_message} =    Confirm
${select_columns_message} =    Select columns
${ok_text_message} =    Ok
${issue_date} =    Issue Date
${pagination_viewpoint} =    View 11 - 20 of 105
${add_all} =    All all
${ascending_order} =    ZZ 0001
${remove_all} =    Remove all
${seventeen_items} =    17 items selected
