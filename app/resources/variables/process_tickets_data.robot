*** Settings ***
Documentation    Where Variables Go To Live For Process Tickets
*** Variables ***
${ticket_number_partial_payment_test} =    E00259
${collections_ticket_number} =    E00075
${partial_payment_status_error_message} =    A partial payment status must be selected.
${date_picker_end_of_feb} =    02/28/2021
${date_picker_from} =    02/01/2021
${invalid_ticket_number} =    IN 9966885577
${dolan_name} =    Dolan
${ticket_number} =    CCC0001
${partial_ticket_number} =    CC
${full_ticket_number} =    E00017
${partial_ticket_number_test_382} =    E005
