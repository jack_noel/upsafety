*** Variables ***

#_______________________________________________________________________________________________________________________
# BROWSERS
#_______________________________________________________________________________________________________________________

${BROWSER_CHROME}                                 chrome
${BROWSER_HEADLESS_CHROME}                        headlesschrome
${BROWSER_FIREFOX}                                firefox
${BROWSER_EDGE}                                   edge

${DELAY}                                          1.25 seconds

#_______________________________________________________________________________________________________________________
# URLS
#_______________________________________________________________________________________________________________________
${TEST_BASE_URL}                                  https://www.tocitestaging.net/qaautomation
${TEST_LOGIN_URL}                                 ${TEST_BASE_URL}
${TEST_DASHBOARD_URL}                             ${TEST_BASE_URL}/Home

${TEST_EXTERNAL_UPS_URL}                          https://www.upsafety.net/
${TEST_EXTERNAL_UPS_SHOP_URL}                     https://shop.upsafety.net/


# DROPDOWNS
#_______________________________________________________________________________________________________________________
# PARKING TICKETS
#_______________________________________________________________________________________________________________________
${TEST_PARKING_TICKETS_URL}
# SETUP
#_______________________________________________________________________________________________________________________
${TEST_DROPDOWN_LIST_DATA_URL}                   ${TEST_BASE_URL}/ClientGenericMaster
${TEST_LOCATIONS_URL}                            ${TEST_BASE_URL}/Location
${TEST_VIOLATIONS_URL}                           ${TEST_BASE_URL}/ClientViolation
${TEST_EXCLUSION_LIST_URL}                       ${TEST_BASE_URL}/ExclusionLicense
${TEST_PAYMENT_SETTINGS_URL}                     ${TEST_BASE_URL}/PaymentSettings
${TEST_ADVANCED_SETUP_URL}                       ${TEST_BASE_URL}/FirstTimeSetup
# USERS/DEVICES
#_______________________________________________________________________________________________________________________
${TEST_MANAGE_USERS_URL}                         ${TEST_BASE_URL}/User
${TEST_MANAGE_USERS_ROLES_URL}                   ${TEST_BASE_URL}/Role
${TEST_MANAGE_USERS_ROLES_ADMIN_PERMSSIONS}      ${TEST_BASE_URL}/Role/GetRoleDetail/2
${TEST_MANAGE_USERS_ROLES_MOBILE}                ${TEST_BASE_URL}/Role/GetRoleDetail/5
${TEST_MANAGE_USER_ROLES_ADMIN_ASSIGN_ROLE}      ${TEST_BASE_URL}/Role/AssignRole/2?roleType=Cloud
${TEST_MANAGE_USER_ROLE_ASSIGN_ROLE_MOBILE}      ${TEST_BASE_URL}/Role/AssignRole/5?roleType=Device
# eCOMMERCE
#_______________________________________________________________________________________________________________________
${TEST_CUSTOMIZE_TICKET_ECOMMERCE_PAGES_URL}     ${TEST_BASE_URL}/TicketSystemConfig
${TEST_ECOMMERCE_REPORTS_URL}                    ${TEST_BASE_URL}/eCommerceReport
# TICKETS
#_______________________________________________________________________________________________________________________
${TEST_PROCESS_TICKETS_URL}                      ${TEST_BASE_URL}/Citation
${TEST_ENTER_PAPER_TICKET_URL}                   ${TEST_BASE_URL}/IssueTicket/PaperTicket
${TEST_NOTICES_URL}                              ${TEST_BASE_URL}/Notice
${TEST_OWNER_DATA_URL}                           ${TEST_BASE_URL}/OwnerData
${TEST_TICKET_REPORTS_URL}                       ${TEST_BASE_URL}/TicketReport
${TEST_IMPORT_TICKET_DATA_URL}                   ${TEST_BASE_URL}/ImportTicketData
# CITATIONS
#_______________________________________________________________________________________________________________________
${TEST_CLOSE_TICKET_TO_CITATION_URL}             ${TEST_BASE_URL}/CloseTicketToCitation
${TEST_UPDATE_CITATION_STATUS_URL}               ${TEST_BASE_URL}/UpdateNewCitation
${TEST_PAPER_CITATION_SETUP_URL}                 ${TEST_BASE_URL}/CitationSetup
${TEST_CITATION_REPORTS_URL}                     ${TEST_BASE_URL}/CitationReport
# CHANGE PASSWORD
#_______________________________________________________________________________________________________________________
${TEST_CHANGE_PASSWORD_URL}                      ${TEST_BASE_URL}/LogOn/ChangePassword
# MISCELLANEOUS
#_______________________________________________________________________________________________________________________
${TEST_BRUSHUP_WIKI_URL}                         ${TEST_BASE_URL}/Wiki/Help
${TEST_EULA_URL}                                https://www.tocitehelp.net/tiki-index.php?page=EULA
#_______________________________________________________________________________________________________________________
