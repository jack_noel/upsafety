*** Settings ***
Documentation    Houses "All" Violations Information

*** Variables ***
#_______________________________________________________________________________________________________________________
# New Viloation Infow Below
#_______________________________________________________________________________________________________________________
${test_new_violation_code} =    Test Code 01
${test_violation_cost} =        100.00
${test_screen_description} =    Test Screen Descripiton
${test_print_description} =     Test Print Descripition
${test_number_of_required_images} =     1

${test_violation_already_exists_error_message} =    Violation Code already exists
#_______________________________________________________________________________________________________________________
${test_meter_violation_code} =    Test Meter Code 01
${test_meter_violation_cost} =    75.00
${test_meter_screen_description} =    This Test Is For A Metered Violation
${test_meter_print_description} =    Testing Print Screen Description For Meter Violation
${test_meter_number_of_required_images} =     1