*** Variables ***
#_______________________________________________________________________________________________________________________
# ENTERING NEW PAPER TICKET TESTING DATA
#_______________________________________________________________________________________________________________________

${test_ticket_number} =    IN
${test_officer_name} =    thor
${test_license_number} =    INDY
${test_date_issued} =    02/14/2021
${test_officer_badge_number} =    021588
${test_street_address} =    6954 Downtown Ave

#_______________________________________________________________________________________________________________________
# ENTERING NEW PAPER TICKET TESTING DATA FOR CLOSING CITATION TICKET
#_______________________________________________________________________________________________________________________

${test_cct_ticket_number} =    CCT
${test_cct_officer_name} =    loki
${test_cct_officer_badge_number} =    089789
${test_cct_license_number} =    cct
${test_cct_street_address} =    1123 Pennsylvania Ave

#_______________________________________________________________________________________________________________________
# ENTERING NEW PAPER TICKET TESTING DATA FOR UPDATING CITATION TICKET
#_______________________________________________________________________________________________________________________

${test_uct_ticket_number} =    UCT
${test_uct_officer_name} =    loki
${test_uct_officer_badge_number} =    089789
${test_uct_license_number} =    uct
${test_uct_street_address} =    1123 New Jersey Street

#_______________________________________________________________________________________________________________________
# ENTERING NEW PAPER TICKET TESTING DATA FOR MODIFYING TEST FOR PROCESS TICKET TESTS
# UPDATE OWNER DETAILS BELOW
#_______________________________________________________________________________________________________________________

${test_modify_add_notes} =    Adding Some Notes To The Notes Field So That There Can Be Some Updates To The Existing Ticket
${test_modify_id_number} =
${test_modify_first_name} =
${test_modify_last_name} =
${test_modify_middle_name} =
${test_modify_address_one} =
${test_modify_address_two} =
${test_modify_city} =
${test_modify_zip} =
${test_modify_country} =
${test_modify_email} =
${test_modify_phone} =
${test_modify_custom_field_one} =
${test_modify_custom_field_two} =
${test_modify_custom_field_three} =
