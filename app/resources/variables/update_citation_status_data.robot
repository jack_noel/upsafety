*** Settings ***
*** Variables ***
${success_message} =    Citation Number updated successfully.
${no_records_text} =    No records to view
${citation_number_data} =    zz999
${valid_citation_number} =    E0000001-1
${invalid_citation_number} =    PA101
${invalid_lp_number} =    3fl1SepQ
${invalid_citation_date} =    05/01/2021
${test_95_data} =    E0000004-4
${invalid_notice_date} =    03/01/2021
${ticket_for_test_98} =    CTC001
${citation_number_for_test_98} =    TST000002-2
${citation_number} =    TST000001-1
${print_friendly_page_title} =    Print Citation Details
${popup_warning_updating_citation_status} =    Include Public Access Copy?
${citation_number_test_110} =    P0000003-3
${invalid_characters_for_test_110} =    City+of+Rockford BW1.jpg
${invalid_characters_message_for_test_110} =    Attachment Name has invalid characters.

${file_folder_for_tests} =    C:/Users/gabys/Desktop/upsafety_docker_v1/upsafety/app/resources/files
${tif_too_large} =    /DSC00019.tif
${too_large_error_message} =    Attachment File Size can not be greater than 1024KB. File Size: [44643KB]
${tif_just_right} =    /Image from iOS (7).jpg

${max_attachments_message} =    The maximum number of attachments (10) has been exceeded.
${test_ticket_data_88} =    E0000010-3

${test_license_plate_data} =    ST5433