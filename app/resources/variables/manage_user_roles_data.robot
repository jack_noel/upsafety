*** Variables ***
#__________________________
# ROLE DATA #
#__________________________
${test_role_name_administrator} =    Administrator
${test_role_error_message} =    Role Name already exists
${test_new_user_role_name} =    Payment Clerk
${test_new_user_role_all} =    All
${test_new_user_role_mobile} =    Issue
${please_confirm_popup_message} =    Please Confirm
