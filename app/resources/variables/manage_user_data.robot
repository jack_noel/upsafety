*** Settings ***
Documentation    Where All "New User" Information Will Live

*** Variables ***
#_______________________________________________________________________________________________________________________
# Cloud User Info Below
#_______________________________________________________________________________________________________________________
${test_login_username} =  June.Noel
${test_first_name} =  June
${test_last_name} =   Noel
${test_station} =    Broadripple
${test_contact_number} =  317-331-2490
${test_email_address} =   jack.noel@t2systems.com
${test_address_one} =     1600 Pennsylvania Avenue
${test_address_two} =     123 Fake Street
${test_zip_code} =        46206
${test_badge_number} =    02212013

#_______________________________________________________________________________________________________________________
# Mobile User Info Below
#_______________________________________________________________________________________________________________________
${test_mobile_user_login} =    1234567
${test_mobile_password} =     123456789
${test_mobile_firstname} =    Fifi
${test_mobile_lastname} =     Noel
${test_mobile_station} =      Broadripple
${test_mobile_contact_number} =    317-123-1324
${test_mobile_email_address} =     jack.noel@t2systems.com
${test_mobile_address_one} =       321 Fake Avenue
${test_mobile_address_two} =       666 Devils Way
${test_mobile_zip_code} =          66606