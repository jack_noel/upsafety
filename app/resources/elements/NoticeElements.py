class Notices:
    AddNewNoticeBtn = "//*[@id='left-nav-new-btn']/span/span"
    SelectTemplateDropDown = "//*[@id='add-notice-template']/option[1]"
    AddNoticeName = "//*[@id='add-notice-name']"
    AddNoticePreview = "//*[@id='notice-preview-placeholder']/div"
    AddNoticeSaveBtn = "//*[@id='add-notice-save-btn']"
    AddNoticeCancelBtn = "//*[@id='add-notice-dlg']/div[2]/div/button[2]"
    # Template Preview Options

    # Existing Notice Page
    NoticeName = "//*[@id='print-tab-notice-name']"
    AppliedFilters = "//*[@id='print-tab-notice-filters-notice-history']"
    EmailNoticesCheckbox = "//*[@id='send-email-letter']"
    # Print Notices Button
    PrintNoticesBtn = "//*[@id='print-notices-btn']"
    PrintNoticesADialog = "//*[@id='print-notice-dlg']"
    PrintNoticesEmailForm = "//*[@id='print-notice-emails']"
    PrintNoticesSendBtn = "//*[@id='print-notice-confirm-btn']"
    MailNoticesBtn = "//*[@id='mail-notices-btn-info']"
    ToolTipBtn = "//*[@id='mail-notice-tooltip-btn']"
    MessageAlertDismissBtn = "//*[@id='print-notice-grid-messages']/span/button"
    # Tickets Eligible for Letter Notice Tab
    NoticesCheckBoxGrid = "//*[@id='cb_print-notice-grid']"
    # Notices Table
    LicensePlateStateAsc = "//*[@id='jqgh_print-notice-grid_licenseState']/span/span[1]"
    LicensePlateStateDesc = "//*[@id='jqgh_print-notice-grid_licenseState']/span/span[2]"
    LicensePlateSearch = "//*[@id='gs_print-notice-grid_licenseState']"
    LicensePlateClearSearch = "//*[@id='gview_print-notice-grid']/div[2]/div/table/thead/tr[2]/th[7]/div/table/tbody/tr/td[3]/a"
    # Pagination
    SelectAllRecords = "//*[@id='jqg1_center']/table/tbody/tr/td[8]/select"
    # Custom Search Bar
    CustomSearchBarBtn = "//*[@id='toggle-custom-search-btn']"
    CustomSearchBarToolTip = "//*[@id='custom-search-tooltip-btn']"
    LastNoticeTypeDropDown = "//*[@id='custom-search-last-notice-type']"
    AllowResendCheckbox = "//*[@id='custom-search-allow-resend']"
    LastNoticeTypeToolTip = "//*[@id='custom-search']/div[1]/div[3]/span/i"
    DaysSinceLastNotice = "//*[@id='custom-search-days-since-last-notice']"
    DaysSinceLastNoticeToolTip = "//*[@id='custom-search']/div[2]/div/div[1]/div[2]/span/i"
    IssueDateFrom = "//*[@id='custom-search-issue-date-from']"
    IssueDateToolTip = "//*[@id='custom-search']/div[3]/div[1]/div[1]/div[2]/span/i"
    IssueDateTo = "//*[@id='custom-search-issue-date-to']"
    IssueDateToolTip = "//*[@id='custom-search']/div[3]/div[2]/div[1]/div[2]/span/i"
    SearchBtn = "//*[@id='custom-search-btn']"
    CloseCustomSearch = "//*[@id='toggle-custom-search-btn']"
    UndoChangesBtn = "//*[@id='custom-search-undo-changes-btn']"