class ExclusionListElements:
    AddExclusionButton = "//*[@id='exclusions-add-btn']"
    OnAuthorityOfTextbox = "//*[@id='exclusions-onAuthorityOf']"
    DescriptionTextbox = "//*[@id='exclusions-name']"
    LicenseStateDropdownDefaultValue = "//*[@id='exclusions-licenseState']"
    LicenseStateDropdownIndianaValue = "//*[@id='exclusions-licenseState']/option[15]"
    LicenseNumberTextbox = "//*[@id='exclusions-licenseNo']"
    NewExclusionSaveButton = "//button[@type='submit']"
    ExclusionHistoryIcon = "//*[@id='history']/i"

    # SEARCH FIELDS WITHIN TABLE #
    DescriptionSearchField = "//*[@id='gs_Name']"
    OnAuthorityOfSearchField = "//*[@id='gs_OnAuthorityOf']"
    LicenseStateSearchField = "//*[@id='gs_LicenseState']"
    LicenseNumberSearchField = "//*[@id='gs_LicenseNo']"
    DeleteIcon = ""
    ActiveDropdownDefaultValue = "//*[@id='gridActive']"
    ActiveDropdownAllValue = "//*[@id='gridActive']/option[1]"
    ActiveDropdownActiveValue = "//*[@id='gridActive']/option[2]"
    ActiveDropdownInactiveValue = "//*[@id='gridActive']/option[3]"

    # TEST VALIDATION ELEMENTS
    NewExclusionValidationElement = "//*[text()='Test Indiana State']"
    TestDeleteIcon = "//*[text()='Test Indiana State']//following::a[2]"
