class PaymentSettingsElements:
    AddLatePaymentFeeButton = "//*[@id='addDialogLatePaymentFee']"
    AfterXDaysTextbox = "//*[@id='DaysExceedIssueDate']"
    HoursTextbox = "//*[@id='HoursExceedIssueDate']"
    LateFeeAmountTextbox = "//*[@id='FineAmount']"
    IsPercentileCheckbox = "//*[@id='IsFinePercentage']"
    AddLatePaymentFeeSaveButton = "//input[@value='Save']"

    #   DISPUTED TICKET EXPLANATIONS TAB
    AddCloudDismissalExplanationsButton = "//*[@id='addDialogGenericReason']"
    CodeTextbox = "//*[@id='ReasonCode']"
    ReasonTextbox = "//*[@id='Reason']"
    DescriptionTextbox = "//*[@id='ReasonDescription']"
    DisputedTicketExplanationSaveButton = "//input[@value='Save']"
    SearchExplanationTypesTextfield = "//*[@id='TextBoxClient']"
    EditIcon = "//*[@id='2']/td[5]/a/i"
    ActiveCheckbox = "//*[@id='2']/td[6]/input"

    #    PAYMENT METHODS TAB
    AddPaymentMethodButton = "//*[@id='addDialogPaymentMethod']"
    AddPaymentMethodNameTextbox = "//*[@id='PaymentMethodName']"
    AddPaymentMethodCodeTextbox = "//*[@id='PaymentMethodCode']"
    AddPaymentMethodCustomizeCheckbox = "//*[@id='Customize']"
    AddPaymentMethodSaveButton = "//input[@value='Save']"
    AddPaymentEditIcon = "//*[@id='1']/td[4]/a/i"
    AddPaymentActiveCheckbox = "//*[@id='1']/td[5]/input"
