class DropdownListDataElements:
    CustomSortOrderButton = "//*[@id='sort-order-btn']"
    AddColorNcIcButton = "//*[@id='add-master-type-btn-label']"
    SearchTextBox = "//*[@id='left-nav-search']"
    # SEARCH INFORMATION DROPDOWNS
    ColorNCICOption = "//*[@id='layout-left-nav']/div[2]/button[1]/div/h5"
    CvbDistrictOption = "//*[@id='layout-left-nav']/div[2]/button[2]/div/h5"
    CvBOffenseTypeOption = "//*[@id='layout-left-nav']/div[2]/button[3]/div/h5"
    LPStateUSCANMEXOption = "//*[@id='layout-left-nav']/div[2]/button[4]/div/h5"
    NewTypeOption = "//*[@id='layout-left-nav']/div[2]/button[5]/div/h5"
    NotesOption = "//*[@id='layout-left-nav']/div[2]/button[6]/div/h5"
    SexOption = "//*[@id='layout-left-nav']/div[2]/button[7]/div/h5"
    VehicleOption = "//*[@id='layout-left-nav']/div[2]/button[8]/div/h5"
    SubVehicleOptionAddVehicle = "//*[@id='add-dialog-btn']"
    VehicleTypeOption = "//*[@id='layout-left-nav']/div[2]/button[9]/div/h5"
    VehicleSearchColumn = "//*[@id='gs_generic-master-grid_KeyName']"
    NewVehicleTypeOption = "//*[@id='layout-left-nav']/div[2]/button[9]/div/h5"
    AddNewVehicleType = "//*[@id='add-master-type-btn-label']"

    LPStateUSCANMEXOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='LPStateUSCANMEX']"
    InformationDropdownNotesOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='Notes']"
    InformationDropdownSexOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='Sex']"
    InformationDropdownVehicleOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='Vehicle']"
    InformationDropdownVehicleTypeOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='VehicleType']"
    InformationDropdownColorOption = "//*[@id='GenericMasterTypeListTemp']/option[text()='ColorNCIC']"

    # SPECIFIC ADD BUTTONS
    ColorNCICOptionButton = "//*[@id='MasterType']"
    LPStateUSCANMEXButton = "//*[@id='addDialog']/span"
    InformationDropdownNotesButton = "//*[@id='addDialog']/span"
    InformationDropdownSexButton = "//*[@id='addDialog']/span"
    InformationDropdownVehicleButton = "//*[@id='addDialog']/span"
    InformationDropdownVehicleTypeButton = "//*[@id='addDialog']/span"


    KeyCode = "//*[@id='key-code']"
    KeyValue = "//*[@id='key-name']"
    KeySaveButton = "//*[@id='generic-master-dlg']/form/div/div[4]/button[1]"
    AddNewInfoCodeForSexOption = "//*[@id='KeyCode']"
    AddNewInfoValueForSexOption = "//*[@id='KeyName']"
    # COLOR NCIC
    AddNewInfoCodeForColorNCICOption = "//*[@id='KeyCode']"
    AddNewInfoValueForColorNCICOption = "//*[@id='KeyName']"
    # NOTES
    AddNewInfoCodeForNotesOption = "//*[@id='KeyCode']"
    AddNewInfoValueForNotesOption = "//*[@id='KeyName']"
    # VEHICLE
    AddNewInfoCodeForVehicleOption = "//*[@id='KeyCode']"
    AddNewInfoValueForVehicleOption = "//*[@id='KeyName']"
    # VEHICLE TYPE
    AddNewInfoCodeForVehicleTypeOption = "//*[@id='KeyCode']"
    AddNewInfoValueForVehicleTypeOption = "//*[@id='KeyName']"
    # LPSTATEUSCANMEX
    AddNewInfoCodeForLPStateOption = "//*[@id='KeyCode']"
    AddNewInfoValueForLPStateOption = "//*[@id='KeyName']"
    # NEW INFORMATION POPUP ELEMENTS
    NewInfoSaveButton = "//input[@type='submit']"
    SearchInformationDropdownTextBox = "//*[@id='TextBoxClient']"
    ActiveDropdownDefaultValue = "//*[@id='gridActive']"
    ActiveDropdownAllValue = "//*[@id='gridActive']/option[1]"
    ActiveDropdownActiveValue = "//*[@id='gridActive']/option[2]"
    ActiveDropdownInActiveValue = "//*[@id='gridActive']/option[3]"
    KeyNameValueAscending = "//*[@id='jqgh_KeyName']/span/span[1]"
    KeyNameValueDescending = "//*[@id='jqgh_KeyName']/span/span[2]"
    PaginationDefaultValue = "//*[@id='globalPaging']"
    PaginationAllValue = "//*[@id='globalPaging']/option[4]"


    # WELCOME TAB
    WelcomeTabMessage = "//*[@id='welcome-tab']/div[2]/div/div[2]"

