class EnterPaperTicketElements:
    TicketNumberTextbox = "//*[@id='CitationNumber']"
    OfficerDropdownDefaultValue = "//*[@id='Officers']"
    OfficerDropdownOtherValue = "//*[@id='Officers']/option[4]"
    OfficerDropdownOtherNameTextbox = "//*[@id='OtherOfficerName']"
    OfficerDropdownOtherBadgeNumberTextbox = "//*[@id='OtherOfficerBadgeNo']"
    IssuedDateTextbox = "//*[@id='IssuedDate']"
    TicketWarningNoRadioButton = "//*[@id='RadioWarningNo']"
    TicketWarningYesRadioButton = "//*[@id='RadioWarningYes']"

    # VEHICLE LICENSE INFO
    LicenseNumberTextbox = "//*[@id='LicenseNo']"
    StateDropdownDefaultValue = "//*[@id='LicenseState']"
    TestStateDropdownIndiana = "//*[@id='LicenseState']/option[15]"

    # LOCATION
    StreetAddressDropdownDefaultValue = "//*[@id='StreetAddress']"
    StreetAddressDropdownOthersValue = "//*[@id='StreetAddress']/option[3]"
    OtherTextbox = "//*[@id='DDText_StreetAddress']"

    # VEHICLE
    VehicleMakeDefaultValue = "//*[@id='Vehicle']"
    TypeDefaultValue = "//*[@id='VehicleType']"
    ColorDefaultValue = "//*[@id='VehicleColor']"

    # TEST VEHICLE DATA ELEMENTS
    TestVehicleSubaru = "//*[text()='Subaru']"
    TestVehicleType = "//*[@id='VehicleType']/option[3]"
    TestVehicleColor = "//*[@id='VehicleColor']/option[5]"

    # VIOLATION
    ViolationDefaultValue = "//*[@id='Tempviolation0']"
    TestCodeViolationHundredDollarFine = "//*[@id='Tempviolation0']/option[2]"

    # NOTES
    NotesDefaultValue = "//*[@id='Notes']"
    TestOffenceValue = "//*[@id='Notes']/option[9]"

    # IMAGES
    ImageDropdownDefaultValue = ""

    # BUTTONS
    AcceptPaperDetailsButton = "//*[@id='btnPaperTicket']"
