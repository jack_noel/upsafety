class LocationElements:
    # [Documentation]
    # Elements for the Location Page within the Upsafety Cloud

    AddLocationButton = "//*[@id='locations-add-btn']"

    # Add New Location Popup #
    DefaultStateDropdown = "//*[@id='locations-state']"
    IndianaStateOption = "//*[@id='locations-state']/option[@value='14']"
    DefaultCityDropdown = "//*[@id='locations-city']"
    IndianapolisCityOption = "//*[@id='locations-city']/option[@value='225']"
    LocationCode = "//*[@id='locations-code']"
    LocationName = "//*[@id='locations-name']"
    IsActiveCheckbox = "//*[@id='locations-dlg']/form/div/div[6]/div/div/label"
    AddNewLocationSaveButton = "//*[@id='locations-dlg']/form/div/div[7]/div/button[1]"
    AddNewLocationCancelButton = "//*[@id='locations-dlg']/form/div/div[7]/div/button[2]"

    # Main Location Container #
    LocationCodeFilter = "//*[@id='gs_locations-grid_locationCode']"
    LocationNameFilter = "//*[@id='gs_locations-grid_locationName']"
    StateFilter = "//*[@id='gs_locations-grid_stateName']"
    CityFilter = "//*[@id='gs_locations-grid_cityName']"
    LocationEditButton = "//*[@id='53']/td[7]/a/i"
    LocationEditColumnsButton = "//*[@id='jqg1_left']/div/div[1]/div/span[1]"
    LocationReportExportCurrentGridButton = "//*[@id='jqg1_left']/div/div[2]/div/span[2]"
    LocationReportExportAllButton = "//*[@id='jqg1_left']/div/div[3]/div/span[2]"
