class ManageUsersElements:
    # [Documentation]
    # Elements for the Manage User Page within the Upsafety Cloud

    # Add Users Button #
    AddUsersButton = "//*[@id='addDialogLarge']/span"

    # Left Hand Side Filter Section / User Type Section #1
    UserTypeMasterDropdown = "//*[@id='UserType']"
    UserTypeAllDropdown = "//*[@id='UserType']/option[1]"
    UserTypeCloudUserDropdown = "//*[@id='UserType']/option[2]"
    UserTypeMobileDeviceDropdown = "//*[@id='UserType']/option[3]"

    # Left Hand Side Filter Section / User Role Section #
    UserRoleAllDropdown = "//*[@id='Role']/option[1]"
    UserRoleAdministratorDropdown = "//*[@id='Role']/option[2]"
    UserRoleOfficerDropdown = "//*[@id='Role']/option[5]"
    UserRoleDeviceInsightsDropdown = "//*[@id='Role']/option[4]"
    UserRoleSuperAdministratorDropdown = "//*[@id='Role']/option[6]"
    UserRoleMobileAllCheckBox = "//*[@id='roles_5']"

    # Add New User Form Section #
    UserFormUserDefaultDropdown = "//*[@id='UserTypeN']"
    UserFormUserTypeCloudUserDropdown = "//*[@id='UserTypeN']/option[1]"
    UserFormUserTypeMobileDeviceDropdown = "//*[@id='UserTypeN']/option[2]"
    UserFormLoginTextbox = "//*[@id='LoginName']"
    UserFormLastNameTextbox = "//*[@id='LastName']"
    UserFormContactNumberTextbox = "//*[@id='ContactNumber']"
    UserFormDepartmentTextbox = "//*[@id='Department']"
    UserFormAddressTwoTextbox = "//*[@id='Address2']"
    UserFormStateDefaultDropdown = "//*[@id='StateID']"
    UserFormStatePennsylvaniaDropdown = "//*[@id='StateID']/option[40]"
    UserFormZipCodeTextbox = "//*[@id='ZipCode']"
    UserFormFirstNameTextbox = "//*[@id='FirstName']"
    UserFormStationTextbox = "//*[@id='Station']"
    UserFormEmailTextbox = "//*[@id='ClientCloudEmailID']"
    UserFormAddressOneTextbox = "//*[@id='Address1']"
    # UserFormCountryDropdown = "//*[@id="CountryID"]"  -- USA is currently the only option #
    UserFormCityDefaultDropdown = "//*[@id='CityID']"
    UserFormBadgeNumberTextbox = "//*[@id='BadgeNumber']"

    # User Form Ticket Categories Section #
    # User Form Ticket Categories Section / Select Modules #
    UserFormSelectModuleToCiteDropdown = "//*[@id='TicketModules']"
    # UserFormSelectModulePermitsDropdown = "" -- Currently No Options #
    # UserFormSelectModulePaForCiteDropdown = "" -- Currently No Options #

    # User Form Ticket Categories Section / Select Module Categories #
    UserFormSelectModuleCategoriesParkingTicketsCheckbox = "//*[@id='ticket_1_1']"

    #  UserFormSelectModuleCategoriesDeferredPaymentSlipCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesEventReceiptsCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesNewYorkParkingCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesPropertyViolationCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesGreenvilleCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesOttawaCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesParkingCvbCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesMassTransitCitationsCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesUniformCitationCheckbox = "" -- Currently No Options #
    #  UserFormSelectModuleCategoriesParkingCitationsCheckbox = "" -- Currently No Options #

    # User Form User Roles Section #

    #  UserRolesAccountingCheckbox = ""
    #  UserRolesAddCheckbox = ""
    #  UserRolesCloudDismissalCheckbox = ""
    #  UserRolesDemoUserCheckbox = ""
    #  UserRolesFinanceClerkTestCheckbox = ""
    #  UserRolesNewRoleCheckbox = ""
    #  UserRolesPaymentClerkCheckbox = ""
    UserRolesDeviceInsightsCheckbox = "//*[@id='roles_4']"
    UserRolesAdministratorCheckbox = "//*[@id='roles_2']"
    UserRolesOfficerCheckbox = "//*[@id='roles_3']"
    UserRolesSuperAdministratorCheckbox = "//*[@id='roles_1']"

    # User Form Buttons #
    UserRoleSaveButton = "//*[@id='btnSave']"
    UserRoleCancelButton = "//*[@id='form1']/table/tbody/tr[12]/td/table/tbody/tr/td[2]/input"

    # User Form Column Header Text #

    UserFormMobilePasswordTextbox = "//*[@id='HandGearPassword']"
    UserFormColumnHeaderLastNameTextbox = "//*[@id='gs_LastName']"
    UserFormColumnHeaderFirstNameTextbox = "//*[@id='gs_FirstName']"
    UserFormColumnHeaderOfficerNumberTextbox = "//*[@id='gs_BadgeNumber']"
    #  UserFormColumnHeaderStationTextbox = "//*[@id="['gs_Station']"
    UserFormColumnHeaderContactNumberTextbox = "//*[@id='gs_ContactNumber']"
    UserFormColumnHeaderEmailTextbox = "//*[@id='gs_EmailId']"
    # UserFormEditButton = "" -- Currently Not Able Due To Incomplete Set Up Of Cloud Instance. #
    # UserFormResetPasswordIcon = "" -- Currently Not Able Due To Incomplete Set Up Of Cloud Instance. #
    # UserFormLockCheckbox = "" -- Currently Not Able Due To Incomplete Set Up Of Cloud Instance. #
    # UserFormDeleteButton = "" -- Currently Not Able Due To Incomplete Set Up Of Cloud Instance. #
    UserFormGlobalPaginationDropdown = "//*[@id='globalPaging']"
    UserFormPaginationTenDropdown = "//*[@id='globalPaging']/option[1]"
    UserFormPaginationTwentyDropdown = "//*[@id='globalPaging']/option[2]"
    UserFormPaginationFiftyDropdown = "//*[@id='globalPaging']/option[3]"
    UserFormPaginationAllDropdown = "//*[@id='globalPaging']/option[4]"
    UserFormCornerCloseButton = "/html/body/div[4]/div[1]/a"

    # Assertion On User Creation Test #
    UserCreationValidationElement = "//*[@title='02212013']"
    duplicated_login_error_message = "//*[text()='Login name already exists']"
    MobileUserCreationValidationElement = "//*[text()='Fifi']"

    # Cleanup Elements #
    DeleteButtonElement = "//*[text()='June']/following::a[4]"
    TestMobileUserDeleteButtonElement = "//*[text()='Fifi']/following::a[3]"
