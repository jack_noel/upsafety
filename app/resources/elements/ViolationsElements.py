class ViolationsElements:
    AddViolationButton = "//*[@id='addDialog']"

    CodeOrNumberTextbox = "//*[@id='LocalViolationCode']"
    ViolationCostTextbox = "//*[@id='ViolationCost']"
    ScreenDescriptionTextbox = "//*[@id='ScreenDescription']"
    PrintDescriptionTextbox = "//*[@id='PrintDescription']"
    ImageDropdownDefaultValue = "//*[@id='ViolationImage']/option[1]"
    ImageDropdownOptional = "//*[@id='ViolationImage']/option[2]"
    ImageDropdownNotAllowed = "//*[@id='ViolationImage']/option[3]"
    NumberOfImagesRequiredTextbox = "//*[@id='NoOfImagesRequired']"
    MeterViolationDropdownDefaultValue = "//*[@id='MeterViolation']/option[2]"
    MeterViolationDropdownYesValue = "//*[@id='MeterViolation']/option[1]"
    DeviceRolesAllCheckbox = "//*[@id='role-5']"
    NewViolationCodeSaveButton = "//input[@value='Save']"

    NewNoneMeterViolationValidation = "//*[text()='Test Code 01']"
    DuplicatedViolationErrorMessage = "//*[@id='displayUIAlert']/span/span"
    NewMeteredViolationValidation = "//*[text()='Test Meter Code 01']"
