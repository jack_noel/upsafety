class ImportTicketDataElements:
    DownloadTemplateButtonElement = "//*[@id='form3']/input[2]"
    DownloadTicketMasterButtonElement = "//*[@id='form2']/input[2]"
    SubmitButtonElement = "//*[@id='btnImportTicketDataSubmit']"
    ChooseFileUploadButtonElement = "//*[@id='fileUpload']"
