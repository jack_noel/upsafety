class ManageUsersRolesElements:
    AddUserRoleButton = "//*[@id='left-nav-new-btn']"
    RoleNameElement = "//*[@id='add-role-name']"
    AddRoleSaveButtonElement = "//*[@id='save-role-name-btn']"
    UserRolesErrorMessage = "//*[@id='displayError']"
    NewUserRoleSaveButton = "//*[@id='content']/form/table/tbody/tr[3]/td/input"
    # TEST VALIDATOR BELOW
    PaymentClerkElement = "//*[text()='Payment Clerk']"
    NewMobileRoleElement = "//*[text()='Issue']"
    NewMobileRoleDeleteButton = "//*[text()='Issue']//following::a[3]"
    PaymentClerkDeleteButtonElement = "//*[text()='Payment Clerk']//following::a[3]"
    RoleTypeDropdown = "//*[@id='add-role-type']"
    RoleTypeDropdownDevice = "//*[@id='add-role-type']/option[2]"
    ActivateIMeterInterface = "//*[@id='jqg_list_96']"
    IssueCheckboxForMobileUser = "//*[@id='jqg_list_10863']"
    CloudActiveIMeterLocationsCheckbox = "//*[@id='jqg_list_101']"
    DevicePermissionIChalkCheckbox = "//*[@id='jqg_list_10862']"
    # CLOUD USER ROLE ASSIGNMENT
    TestJuneUserRoleCheckbox = "//*[@id='jqg_list_9']"
    AssignRoleSaveButton = "//*[text()='Save']"
    # MOBILE USER ROLE ASSIGNMENT
    TestFifiMobileDeviceCheckbox = "//*[@id='jqg_list_14']"
    TableDropdownDefaultValue = "//*[@id='globalPaging']"
    TableDropdownAllValue = "//*[@id='globalPaging']/option[4]"

    RoleAlreadyExistsErrorMessageElement = "//*[text()='Role name already exists.']"
    RoleSearchElement = "//*[@id='left-nav-search']"
