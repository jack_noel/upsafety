class ProcessTicketsElements:
    SearchATicketButton = "//*[@id='btnSubmitRefreshGrid']"
    AcceptPaymentsButton = "//*[@id='ticket-payment-dlg']/div/div[2]/div/button[1]"
    PartialPaymentStatusErrorMessageElement = "//*[text()='A partial payment status must be selected.']"
    TotalAmountPaymentErrorMessageElement = "//*[text()='Total amount must be greater than $0.']"
    PartialPaymentReasonErrorMessageElement = "//*[text()='A partial payment reason must be entered.']"
    APaymentMethodMustBeSelectedErrorMessageElement = "//*[text()='A payment method must be selected.']"
    TicketNumberForCollectionsElement = "//*[text()='E00075']"
    TicketNumberForHearingStatusElement = "//*[text()='ZZ 0001']"
    TicketNumberForExportStatusElement = "//*[text()='ZZ 0001']"
    TicketNumberForOpenIssuedElement = "//*[text()='ZZ 0001']"
    OpenPartialPaymentTicketElement = "//*[text()='E00046']"
    InDisputeTicketElement = "//*[text()='E00068']"
    ClosedTicketElement = "//*[text()='IN 9635159']"
    IssuedToDatePickerElement = "//*[@id='To']"
    IssuedFromDatePickerElement = "//*[@id='From']"
    ClosedTicketIssuedToElement = "//*[text()='IN 9160285']"
    TicketNumberElement = "//*[@id='TicketNumber']"
    NoRecordsToViewElement = "//*[text()='No records to view']"
    LicenseNumberTextBoxElement = "//*[@id='LicenseNumber']"
    LicenseNumberElement = "//*[text()='CCC0001']"
    # ENTER PAPER TICKET TEST DATA #
    SeventyFiveDollarFineElement = "//*[text()='$75.00']"

    # TICKET TABLE AND DETAIL #
    TicketNumberTextbox = "//*[@id='gs_CitationNumber']"
    TicketNumberCheckbox = "//*[@id='jqg_list_53']"
    PayTicketButtonElement = "//*[@id='batchPayBtn']"
    AmountReceivedTextBoxElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[1]/td[10]/table/tbody/tr/td[2]/input"
    OwnerNameTextboxElement = "//*[@id='OwnerName']"

    # PAYMENT DROP DOWNS SELECTIONS #
    PaymentMethodDefaultValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select"
    PaymentMethodDropdownCashValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[2]"
    PaymentMethodDropdownCheckValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[3]"
    PaymentMethodDropdownCreditCardValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[4]"
    PaymentMethodDropdownDebitCardValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[5]"
    PaymentMethodDropdownStripeValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[6]"
    PaymentMethodDropdownStudentAccountValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[7]"
    PaymentMethodDropdownTerminalEmulatorValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[4]/td[2]/select/option[8]"

    # PARTIAL PAYMENT DROP DOWN SELECTIONS #
    PartialPaymentStatusDefaultValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[6]/td[2]/select"
    PartialPaymentStatusClosedValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[6]/td[2]/select/Option[2]"
    PartialPaymentStatusOpenPartialPaymentValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[6]/td[2]/select/Option[3]"
    PartialPaymentStatusOpenPaymentOpenValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[6]/td[2]/select/Option[4]"

    # PARTIAL PAYMENT REASON DROP DOWN SELECTION #
    PartialPaymentReasonDefaultValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[7]/td[2]/select"
    PartialPaymentReasonOtherValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[7]/td[2]/select/option[2]"
    PartialPaymentReasonNotEnoughCashValueElement = "//*[@id='ticket-payment-dlg']/div/table/tbody/tr[7]/td[2]/select/option[3]"

    # SEARCH A TICKET FORM ELEMENTS #
    TicketStateDefaultValueElement = "//*[@id='NewTicketStateType']"
    TicketStateOpenValueElement = "//*[@id='NewTicketStateType']/option[2]"
    TicketStateClosedValueElement = "//*[@id='NewTicketStateType']/option[3]"
    TicketStateRmsExportValueElement = "//*[@id='NewTicketStateType']/option[4]"
    TicketStateHearingValueElement = "//*[@id='NewTicketStateType']/option[5]"

    TicketStatusDefaultValueElement = "//*[@id='NewTicketStateID']"
    TicketStatusIssuedValueElement = "//*[@id='NewTicketStateID']/option[2]"
    TicketStatusInDisputeValueElement = "//*[@id='NewTicketStateID']/option[3]"
    TicketStatusPartialPaymentValueElement = "//*[@id='NewTicketStateID']/option[4]"
    TicketStatusPaymentPlanValueElement = "//*[@id='NewTicketStateID']/option[5]"
    TicketStatusCollectionsValueElement = "//*[@id='NewTicketStateID']/option[6]"

    ResetButtonElement = "//*[@id='btnReset']"
    NewTicketStateTypeAllElement = "//*[@id='NewTicketStateType']"
    PartialLicenseNumberElement = "//*[text()='E01253']"
    FullTicketNumberElement = "//*[text()='E00017']"
    PartialTicketNumberElement = "//*[text()='E00504']"

    EditTicketEllipsisElement = "//*[@id='btnTicketDetailMenu']/span"
    HistoryEditButtonElement = "//*[@id='btnTicketDetailHistory']/span[1]"
    LicenseStateCodeTextElement = "//*[text()='License State Code']"
    ZZ0001Element = "//*[@id='ticketNumLink-156']"

    HistoryEllipsisElement = "//*[@id='demo-menu-lower-right']/i"
    HistoryExpandAllButtonElement = "//*[@id='changeHistoryDialog']/div[1]/div[2]/ul/li[1]"
    HistoryCollapseAllButtonElement = "//*[@id='changeHistoryDialog']/div[1]/div[2]/ul/li[2]"

    LicenseStateCodeElement = "//*[@id='changeHistoryDialog']/div[2]/ul/li/div[2]/div/div/span[3]"
    HistoryDropdownElement = "//*[@id='changeHistoryDialog']/div[2]/ul/li/div[1]/span"

    TicketDetailResetFeeDiscountElement = "//*[@id='btnResetLateFee']/span[1]"
    TicketDetailResetDisputeDateElement = "//*[@id='btnResetDisputeDate']"
    TicketDetailNotesElement = "//*[@id='btnAddNotes']/span[1]"
    TicketDetailAddAttachmentElement = "//*[@id='attachmentPlaceholder']/span"
    TicketDetailSetAlertElement = "//*[@id='btnSetAlert']"
    TicketDetailEditElement = "//*[@id='btnEdit']/span[1]"
    TicketDetailHistoryElement = "//*[@id='btnTicketDetailHistory']/span[1]"