class LoginElements:
    # [Documentation]
    # All the elements that are used within the Login Test for the Upsafety Cloud
    # login page elements
    login_Username = "//*[@id='username']"
    Login_Password = "//*[@id='password']"
    Login_Button = "//*[@id='logon']/div/button[1]"
    Forgot_Password_Button = "//*[@id='logon']/div/button[2]"
    LoginPageTitle = "/html/head/title"
    ErrorMessage = "//div[@class='loginError']"
