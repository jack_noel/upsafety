*** Settings ***
Documentation

Library    SeleniumLibrary
Library    String
Library    OperatingSystem

Resource    ../variables/urls.robot
Resource    ../variables/owner_data_data.robot

Variables    ../elements/OwnerDataElements.py

*** Keywords ***
Navigate To Owner Data Page
	[Documentation]    Admin Is Navigating To Owner Data Page
	...                Then waiting for an element to be visible before continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_OWNER_DATA_URL}
	Wait Until Element Is Visible    ${OwnerDataSearchButton}

Post Test File Removal For Michigan
	${FILE_PATH} =    catenate    SEPARATOR=    ${test_current_dir_export_file_for_MI}    ${test_name_of_exported_file_for_MI}
	Remove File    ${FILE_PATH}

Post Test File Removal For Penn
	${FILE_PATH} =    catenate    SEPARATOR=    ${test_current_dir_export_file_for_Penn}    ${test_name_of_exported_file_for_Penn}
	Remove File    ${FILE_PATH}


Identifying Required Values For Search Feature
	[Documentation]    Admin clicking the search button without populating the textfields
	...                Looking for the required field message to appear
	...                === END DOCUMENTATION ===
	Wait Until Element Is Visible    ${OwnerDataSearchButton}    10
	Click Element    ${OwnerDataSearchButton}
	Element Should Contain    ${RequiredValueOneElement}   ${required_values_upon_search_click}
	Element Should Contain    ${RequiredValueTwoElement}   ${required_values_upon_search_click}

Search By Tickets Issued From Date
	Input Text    ${TicketIssuedFromDateElement}   ${test_ticket_issued_from_date}
	Input Text    ${TicketIssuedToDateElement}   ${test_ticket_issue_to_date}
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataExists}
	Click Element    ${OwnerDataSearchButton}

Assert Results Are Displayed Correctly When Searching By ODE
	Wait Until Element Is Visible    ${OwnerDataNeededResultOne}
	Element Should Be Visible    ${OwnerDataNeededResultOne}

Current Frame Is Should Be Alert
	Current Frame Should Contain    Alert
	Wait Until Element Is Visible    //*[@id="popup_ok"]
	Element Should Be Visible        //*[@id="popup_ok"]
	Click Element                   //*[@id="popup_ok"]

Search By Radio Button With One Value
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchButton}

Post Test Edit Owner Info
	[Documentation]
	${random_last_name} =    Generate Random String    6    [LETTERS]
	${random_four_digit} =    Generate Random String    4    [NUMBERS]
	${random_address} =    Catenate    ${random_four_digit}    ${fake_address_street}
	${random_zip_code} =    Generate Random String    6    [NUMBERS]
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataNeeded}
	Click Element    ${OwnerDataSearchButton}
	Scroll Element Into View    //*[@title='IN 0984109064']//following::a[1]
	Click Element    //*[@title='IN 0984109064']//following::a[1]
	Input Text    ${OwnerLastNameElement}   ${random_last_name}
	Input Text    ${OwnerAddressLineOneElement}   ${random_address}
	Input Text    ${OwnerCityElement}   ${test_owner_city}
	Click Element    ${OwnerStateDefaultValueElement}
	Click Element    ${OwnerStateIndianaValueElement}
	Input Text    ${OwnerStateZipCodeElement}   ${random_zip_code}
	Click Element    ${EditOwnerDataSaveButton}
	Reload Page
	Click Element    ${UnpaidDaysRadioButton}
	Input Text    ${UnpaidDaysTextbox}    1
	Click Element    ${OwnerDataSearchTypeDefaultValue}
	Click Element    ${OwnerDataSearchTypeOwnerDataExists}
	Click Element    ${OwnerDataSearchButton}
	Wait Until Element Is Visible    //*[@title='IN 0984109064']
	Element Should Be Visible    //*[@title='IN 0984109064']
	Capture Page Screenshot    EMBED

