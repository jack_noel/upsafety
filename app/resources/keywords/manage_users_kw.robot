*** Settings ***
Documentation    Methods, Functions For User Management Are Housed Here

Library      SeleniumLibrary

Resource    ../variables/manage_user_data.robot
Resource    ../variables/urls.robot

Variables    ../elements/ManageUsersElements.py

*** Keywords ***
Navigate To User Management Page
	[Documentation]    Admin Is Navigating To User Management Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_MANAGE_USERS_URL}
	Wait Until Element Is Visible    ${AddUsersButton}

Click Add New Users Button
	[Documentation]    Admin Is Clicking On The Add Users Button
	...                === END DOCUMENTATION ===
	Click Element    ${AddUsersButton}

Input Data Into All Required Fields For Cloud User
	[Documentation]    Admin Is Inputting All The Required Fields For Adding A New User To The Cloud. These Are The Following:
	...                Username, First Name, Last Name, Station, Contact Number, Email Address, Address One, Two, Badge Number, Zip Code
	...                Certain Checkboxes, Administrator, Manager... And Module Parking Tickets Checkbox
	...                === END DOCUMENTATION ===
	Input Text    ${UserFormLoginTextbox}    ${test_login_username}
	Input Text    ${UserFormFirstNameTextbox}    ${test_first_name}
	Input Text    ${UserFormLastNameTextbox}    ${test_last_name}
	Input Text    ${UserFormStationTextbox}    ${test_station}
	Input Text   ${UserFormContactNumberTextbox}    ${test_contact_number}
	Input Text   ${UserFormEmailTextbox}    ${test_email_address}
	Input Text   ${UserFormAddressOneTextbox}    ${test_address_one}
	Input Text   ${UserFormAddressTwoTextbox}    ${test_address_two}
	Input Text   ${UserFormBadgeNumberTextbox}    ${test_badge_number}
	Input Text   ${UserFormZipCodeTextbox}    ${test_zip_code}
	Click Element    ${UserRolesAdministratorCheckbox}
	Click Element    ${UserFormSelectModuleCategoriesParkingTicketsCheckbox}
	Click Element    ${UserRoleSaveButton}

Input Data Into All Required Fields For Mobile User Creation
	[Documentation]    Admin Is Inputting All The Required Fields For Adding A New User To A Mobile Device. These Are The Following:
	...                Username, Password, First Name, Last Name, Station, Contact Number, Email Address, Address One, Two, Badge Number, Zip Code
	...                Certain Checkboxes, Administrator, Manager... And Module Parking Tickets Checkbox
	...                === END DOCUMENTATION ===
	Click Element    ${UserFormUserDefaultDropdown}
	Click Element    ${UserFormUserTypeMobileDeviceDropdown}
	Input Text    ${UserFormLoginTextbox}    ${test_mobile_user_login}
	Input Text    ${UserFormMobilePasswordTextbox}    ${test_mobile_password}
	Input Text    ${UserFormFirstNameTextbox}    ${test_mobile_firstname}
	Input Text    ${UserFormLastNameTextbox}    ${test_mobile_lastname}
	Input Text    ${UserFormStationTextbox}    ${test_mobile_station}
	Input Text   ${UserFormContactNumberTextbox}    ${test_mobile_contact_number}
	Input Text   ${UserFormEmailTextbox}    ${test_mobile_email_address}
	Input Text   ${UserFormAddressOneTextbox}    ${test_mobile_address_one}
	Input Text   ${UserFormAddressTwoTextbox}    ${test_mobile_address_two}
	Input Text   ${UserFormZipCodeTextbox}    ${test_mobile_zip_code}
	Click Element    ${UserRoleMobileAllCheckBox}
	Click Element    ${UserFormSelectModuleCategoriesParkingTicketsCheckbox}
	Click Element    ${UserRoleSaveButton}

Delete User Cleanup
	[Documentation]    Post Test Cleanup, System Is Deleting New Cloud User.
	...                === END DOCUMENTATION ===
	Click Element    ${DeleteButtonElement}

Delete Test Mobile User For Test Cleanup
	[Documentation]    Post Test Cleanup, System Is Deleting New Mobile User.
	...                === END DOCUMENTATION ===
	Click Element    ${TestMobileUserDeleteButtonElement}

Input Duplicated Data For Cloud User
	[Documentation]    Admin Testing Duplication Error Message, Anticipating Getting An Error When There's A Duplicate Username.
	...                === END DOCUMENTATION ===
	Input Text    ${UserFormLoginTextbox}    ${test_login_username}
	Input Text    ${UserFormFirstNameTextbox}    ${test_first_name}