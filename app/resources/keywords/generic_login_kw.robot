*** Settings ***
Documentation    When not testing the login process, this keyword should be used.
Library     SeleniumLibrary

Resource    ../../resources/variables/urls.robot
Resource    ../../resources/variables/login_data.robot

Variables   ../../resources/elements/LoginElements.py

*** Keywords ***
Admin Logging In
	[Documentation]
	Input Text    ${LoginUsername}   ${test_username}
	Input Password    ${LoginPassword}   ${test_password}
	Click Button    ${LoginButton}
	Location Should Be    ${TEST_DASHBOARD_URL}
