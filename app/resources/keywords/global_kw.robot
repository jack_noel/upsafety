*** Settings ***
Documentation
Library    SeleniumLibrary
Library    String

Resource    ../variables/urls.robot
Resource    ../variables/global_data.robot

Variables   ../elements/LoginElements.py
Variables   ../elements/GlobalElements.py

*** Keywords ***
Test Initialization
	[Documentation]     Test Suite Initialization
	...                 This is where the launching of the browser and navigating to the login page will occur.
	...                 === END DOCUMENTATION ===
	Open Browser    ${TEST_BASE_URL}   ${BROWSER_CHROME}
	Set Selenium Speed    ${DELAY}


Test Init Headless
	[Documentation]     Test Suite Initialization
	...                 This is where the launching of the browser and navigating to the login page will occur.
	...                 === END DOCUMENTATION ===
	Open Browser    ${TEST_BASE_URL}   ${BROWSER_HEADLESS_CHROME}
	Set Selenium Speed    ${DELAY}

Kill Browser
	[Documentation]    Terminating the chromedriver instance.
	...                === END DOCUMENTATION ===
	Delete All Cookies
	Close All Browsers

Confirmation Warning Popup
	[Documentation]    Locates the current frame popup, in this case a warning. Ensures Element is Visible.
	...                === END DOCUMENTATION ===
	Current Frame Should Contain    Warning
	Element Should Be Visible    ${PopupOkButton}

Confirmation Alert Popup
	[Documentation]    Locates the current frame popup, in this case a warning. Ensures Element is Visible.
	...                === END DOCUMENTATION ===
	Current Frame Should Contain    Alert

Confirmation Warning Popup, Then Click OK
	[Documentation]    Locates the current frame popup, in this case a warning. Ensures Element is Visible.
	...                === END DOCUMENTATION ===
	Current Frame Should Contain    Warning
	Element Should Be Visible    ${PopupOkButton}
	Click Element    ${PopupOkButton}

Get Screenshot
	Capture Page Screenshot    ${SCREENSHOT_ORIENTATION}
