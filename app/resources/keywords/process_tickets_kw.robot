*** Settings ***
Library    SeleniumLibrary

Resource    ../variables/process_tickets_data.robot
Resource    ../keywords/global_kw.robot

Variables   ../elements/ProcessTicketsElements.py

*** Keywords ***
Naviagte To Process Tickets Page
    [Documentation]    Admin goes to the Process Tickets Page, then waits for the search button to be visible
    Go To    ${TEST_PROCESS_TICKETS_URL}
    Wait Until Element Is Visible    ${SearchATicketButton}

Narrowing Ticket Search
    [Documentation]    Although this is part of a single test, this could be modified to be useful later.
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Input Text    ${TicketNumberTextbox}    ${ticket_number_partial_payment_test}
    Press Keys    ${TicketNumberTextbox}    RETURN
    Select Checkbox    ${TicketNumberCheckbox}
    Click Element    ${PayTicketButtonElement}

Narrowing Ticket Search For Collections
    [Documentation]    Narrowing The Search for the Collections Test
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${TicketStatusCollectionsValueElement}
    Click Element    ${SearchATicketButton}

Narrowing Ticket Search For Open Payment Plan
    [Documentation]    Narrowing The Search for the Open Payment Plan Test
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${TicketStatusPaymentPlanValueElement}
    Click Element    ${SearchATicketButton}

Narrowing Ticket Search For RMS Exports Ticket State
    [Documentation]    Narrowing The Search for the RMS Exports Ticket State Test
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateRmsExportValueElement}
    Click Element    ${SearchATicketButton}

Narrowing Ticket Search To Hearing Status
    [Documentation]    Narrowing The Search for the Hearing Status Test
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateHearingValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${SearchATicketButton}

Changing Amount, Then Selecting A Partial Payment Reason
    [Documentation]    Narrowing The Search for the  Partial Payment Reason Test
    Sleep    5    reason=None
    Wait Until Element Is Visible    ${AmountReceivedTextBoxElement}
    Clear Element Text    ${AmountReceivedTextBoxElement}
    Input Text    ${AmountReceivedTextBoxElement}    2.00
    Click Element    ${PaymentMethodDefaultValueElement}
    Click Element    ${PaymentMethodDropdownCashValueElement}
    Click Element    ${PartialPaymentReasonDefaultValueElement}
    Click Element    ${PartialPaymentReasonOtherValueElement}
    Click Element    ${AcceptPaymentsButton}

Changing Amount To Zero
    [Documentation]    See method name, lol.
    Sleep    5    reason=None
    Wait Until Element Is Visible    ${AmountReceivedTextBoxElement}
    Clear Element Text    ${AmountReceivedTextBoxElement}
    Input Text    ${AmountReceivedTextBoxElement}    0.00
    Click Element    ${AcceptPaymentsButton}

Changing Amount, Then Selecting A Partial Payment
    [Documentation]    See method name, lol.
    Sleep    5    reason=None
    Wait Until Element Is Visible    ${AmountReceivedTextBoxElement}
    Clear Element Text    ${AmountReceivedTextBoxElement}
    Input Text    ${AmountReceivedTextBoxElement}    2.00
    Click Element    ${PartialPaymentReasonDefaultValueElement}
    Click Element    ${PartialPaymentReasonOtherValueElement}
    Click Element    ${AcceptPaymentsButton}

Selecting Save Before Selecting A Payment Method
    Sleep    5    reason=None
    Wait Until Element Is Visible    ${AmountReceivedTextBoxElement}
    Click Element    ${AcceptPaymentsButton}

Selecting Ticket State Option, And Changing It To Partial Payment
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${TicketStatusPartialPaymentValueElement}
    Click Element    ${SearchATicketButton}

Selecting Open And Disputed Options
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${TicketStatusInDisputeValueElement}
    Click Element    ${SearchATicketButton}

Searching For A Closed Ticket
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateClosedValueElement}
    Click Element    ${SearchATicketButton}

Searching For Opened And Issued Ticket
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${TicketStatusDefaultValueElement}
    Click Element    ${TicketStatusIssuedValueElement}
    Click Element    ${SearchATicketButton}

Searching For All Tickets That Are Open
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}
    Click Element    ${SearchATicketButton}

Searching For All Tickets That Are Open, But Not Clicking The Search Button
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${TicketStateDefaultValueElement}
    Click Element    ${TicketStateOpenValueElement}

Searching By Issue Date To Field, And Searching
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${IssuedToDatePickerElement}    ${date_picker_end_of_feb}
    Click Element    ${SearchATicketButton}
    Click Element    ${PopupOkButton}

Searching By Issue Date From Field, And Searching
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${IssuedFromDatePickerElement}    ${date_picker_from}
    Click Element    ${SearchATicketButton}

Searching For Invalid For No Records Viewing
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${TicketNumberElement}    ${invalid_ticket_number}
    Click Element    ${SearchATicketButton}

Searching For Owner Name
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${OwnerNameTextboxElement}    ${dolan_name}
    Click Element    ${SearchATicketButton}

Searching By License Number
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${LicenseNumberTextBoxElement}    ${ticket_number}
    Click Element    ${SearchATicketButton}

Searching By Partial License Number
    Wait Until Element Is Enabled    ${SearchATicketButton}
    Input Text    ${LicenseNumberTextBoxElement}    ${partial_ticket_number}
    Click Element    ${SearchATicketButton}

Searching By Ticket Number
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${TicketNumberElement}    ${full_ticket_number}
    Click Element    ${SearchATicketButton}

Searching By Partial Ticket Number
    Wait Until Element Is Visible    ${SearchATicketButton}
    Input Text    ${TicketNumberElement}    E005
    Click Element    ${SearchATicketButton}

Clicking Search Without Populating Any Form Data
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}

Viewing History Through Edit Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Scroll Element Into View    ${HistoryEditButtonElement}
    Click Element    ${HistoryEditButtonElement}

Expand All History
    Wait Until Element Is Visible    ${HistoryEllipsisElement}
    Click Element    ${HistoryEllipsisElement}
    Click Element    ${HistoryExpandAllButtonElement}

Collapse History
    Wait Until Element Is Visible    ${HistoryEllipsisElement}
    Click Element    ${HistoryEllipsisElement}
    Click Element    ${HistoryCollapseAllButtonElement}

Viewing Reset Fee Through Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Click Element    ${TicketDetailResetFeeDiscountElement}

View Reset Dispute Date Through Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Click Element    ${TicketDetailResetDisputeDateElement}

View Notes Through Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Click Element    ${TicketDetailNotesElement}

Set Alert Through Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Click Element    ${TicketDetailSetAlertElement}

Edit Actual Ticket Through Ticket Detail
    Wait Until Element Is Visible    ${SearchATicketButton}
    Click Element    ${SearchATicketButton}
    Confirmation Warning Popup, Then Click OK
    Wait Until Element Is Visible    ${ZZ0001Element}
    Click Element    ${ZZ0001Element}
    Wait Until Element Is Visible    ${EditTicketEllipsisElement}
    Click Element    ${EditTicketEllipsisElement}
    Click Element    ${TicketDetailEditElement}