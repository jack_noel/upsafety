*** Settings ***
Documentation
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../variables/urls.robot
Resource    ../variables/update_citation_status_data.robot

Variables   ../elements/UpdateCitationStatusElements.py
*** Keywords ***
Navigate To Update New Citation Page
	Go To    ${TEST_UPDATE_CITATION_STATUS_URL}
	#Wait Until Element Is Visible    ${CitationDateTextbox}

Adding Attachments Inside Citation Detail
	${PATH} =    catenate    SEPARATOR=    ${file_folder_for_tests}    ${tif_too_large}
	Choose File    ${ChooseFileInputElement}    ${PATH}

Assert Proper Error Message Is Triggered
	Element Text Should Be    ${AttachmentErrorMessageWarning}    ${too_large_error_message}

Adding Appropriate Sized File Inside Citation Detail
	${PATH} =    catenate    SEPARATOR=    ${file_folder_for_tests}    ${tif_just_right}
	Choose File    ${ChooseFileInputElement}    ${PATH}

Assert File Is Uploaded Correctly
    Click Element    ${CitationNumberGridResultElement}
    Click Element    ${EditButtonElement}
    Element Should Be Visible    ${NewestAttachmentElement}

Post Test Clean Up For 108
	Click Element    //*[@id="td-ticket-attachments"]/fieldset/table/tbody/tr[10]/td[5]/button[2]
	Click Element    ${CitationDetailSaveButton}

Clicking Save Citation Detail Button
	Click Element    ${CitationDetailSaveButton}

Getting And Asserting Text Should Be The Success Message
	Get Text    ${SuccessfulSavedCitationToastMessageElement}
	Element Text Should Be    ${SuccessfulSavedCitationToastMessageElement}    ${success_message}

Post Test Clean Up For 114
	Remove File    C:/Users/gabys/Downloads/Stag Rockburg Citizen Portal.pdf
	File Should Not Exist    C:/Users/gabys/Downloads/Stag Rockburg Citizen Portal.pdf

Post Test Clean Up For 116
	Click Element    ${GridCheckBoxElementForTest116a}
	Click Element    ${GridCheckBoxElementForTest116b}
	Click Element    ${ActionToPerformDropdownDefaultStatus}
	Click Element    ${ActionToPerformDropdownOpenStatus}
	Click Element    ${ActionToPerformSaveButton}
	Element Text Should Be    //*[text()='P0000001-1']//following::td[2]    Open

Click Ticket Search Button
	Click Element    ${SearchATicketSearchButton}

Click The Citation Element
	Click Element   //*[text()='${citation_number}']

Click Ticket Toolbar Button
	Click Element   ${TicketToolBarButtonElement}

Asserting That Test 101 Element Is Visible
	Element Should Be Visible    ${CitationNumberNumberElement}

Clicking The Main Content Button
	Click Element   ${MainContentButton}

Click Print Button
	Click Element   ${CitationPrintFriendlyPrintButton}

Selecting The Checkboxs For Test 100
	Select Checkbox     //*[@id="chkColumn_16"]
	Select Checkbox     //*[@id="chkColumn_15"]

Click Action To Perform Save Button
	Click Element    ${ActionToPerformSaveButton}

Populate Ticket Information
	Input Text    ${CitationNumberTextbox}    ${test_ticket_data_88}

Populate Citation Number For Test 107
	Input Text    ${CitationNumberTextbox}    E0000010-3

Clicking The Edit Button
	Click Element    ${EditButtonElement}

Clicking The Edit Button Using Id
	Click Element    //*[@id='${EditButtonElement}']

Select Citation Open Status And Searching
	Click Element    ${CitationStatusDropdownDefaultStatus}
	Click Element    ${CitationStatusDropdownOpenStatus}
	Click Element    ${SearchATicketSearchButton}

Scrolling Element Into View
	Scroll Element Into View    ${AttachmentsElement}

Asserting Attachment Element Exists
	Element Should Be Visible    ${AttachmentsElement}
Select Citation Payment Plan Status And Searching
	Click Element    ${CitationStatusDropdownDefaultStatus}
	Click Element    ${CitationStatusDropdownPaymentPlanStatus}
	Click Element    ${SearchATicketSearchButton}

Click Ticket Search Button Without Options
	Click Element    ${SearchATicketSearchButton}

Assert Citation Number Appears For Test 88
	Element Should Be Visible    ${CitationNumberResultElementForTest88}

Populate Invalid Citation Number
	Input Text    ${CitationNumberTextbox}    ${citation_number_data}

Asserting That No Records Are Displayed
	Element Should Be Visible    //*[text()="${no_records_text}"]

Populate Valid Citation Number
	Input Text    ${CitationNumberTextbox}    ${valid_citation_number}

Asserting That Valid Record Is Displayed
	Element Should Be Visible    //*[text()="${valid_citation_number}"]

Populate Invalid Ticket Number
	Input Text    ${TicketNumberTextbox}    ${invalid_citation_number}

Populating License Plate Number
	Input Text    ${LicensePlateNumberTextbox}    ${test_license_plate_data}

Asserting Reset Button Is Present
	Element Should Be Visible    ${ResetButton}

Asserting Citation Number Results Are Present
	Element Should Be Visible    ${CitationNumberResultsElement}

Populating Invalid License Plate Number And Searching
	Input Text    ${LicensePlateNumberTextbox}    ${invalid_lp_number}
	Click Element    ${SearchATicketSearchButton}

Selecting Invalid Citation Date And Searching
	Input Text    ${CitationDateTextbox}    ${invalid_citation_date}
	Click Element    ${SearchATicketSearchButton}

Clicking Main Content Button
	Click Button    ${MainContentButton}

Selecting Grid Checkbox
	Select Checkbox    ${GridCheckbox}

Click The Ok Text
	Click Element    ${OkTextElement}

Asserting Test 95 Data Is Displayed
	Element Should Be Visible    //*[text()='${test_95_data}']

Populating Invalid Notice Date
	Input Text    ${NoticeDateTextbox}    ${invalid_notice_date}

Selecting Excused Status And Searching
	Click Element    ${CitationStatusDropdownDefaultStatus}
	Click Element    ${CitationStatusDropdownExcusedStatus}
	Click Element    ${SearchATicketSearchButton}

Populating Ticket Number Textbox With Test Data 98 And Searching
	Input Text    ${TicketNumberTextbox}    ${ticket_for_test_98}
	Click Element    ${SearchATicketSearchButton}

Asserting Test 98 Data Is Displayed
	Element Should Be Visible    //*[text()='${citation_number_for_test_98}']

Asserting Test Data For 98 Doesn't Contain Ticket Number
	Element Should Not Contain    ${TicketNumberElement}   ${ticket_for_test_98}

Clicking Reset Button
	Click Button    ${ResetButton}

Asserting Test 100 Element Isn't Visible
	Click Element    //div[@class='ui-dialog-buttonset']//following::button
	Element Should Not Be Visible    //*[@id="ui-dialog-title-colchooser_list"]

Switching To New Window
	Switch Window   NEW

Asserting Printer Page Title Is Print Citation Details
	Title Should Be    ${print_friendly_page_title}

Click Button For Test 103
	Click Element    ${ButtonWithinTableForTest103}

Asserting Frame Will Contain Citation Status
	Current Frame Should Contain    ${popup_warning_updating_citation_status}

Clicking Cancel Button Post Test 103
	Click Element    ${PopupCancelButtonElement}

