*** Settings ***
Documentation    Methods Live Here
Library     SeleniumLibrary

Resource    ../variables/login_data.robot
Resource    ../variables/urls.robot

Variables   ../elements/LoginElements.py

*** Keywords ***
Admin Logging In
	[Documentation]    Generic New Admin Logging In.
	...                This includes Inputing Text, Password, Clicking Login Button
	...                === END DOCUMENTATION ===
    Input Text    ${LoginUsername}   ${test_username}
	Input Password    ${LoginPassword}   ${test_password}
	Click Button    ${LoginButton}
	Location Should Be    ${TEST_DASHBOARD_URL}

Admin Inputs Incorrect Password
	[Documentation]    Generic New Admin Failing to Log in.
	...                This includes Inputing Text, Invalid Password, Clicking Login Button, And Asserting That Error Message Is Displayed
	...                === END DOCUMENTATION ===
	Input Text    ${LoginUsername}   ${test_username}
	Input Password    ${LoginPassword}   ${invalid_password}
	Click Button    ${LoginButton}
	Element Should Be Visible    ${ErrorMessage}    ${loging_error_message}

	# You want to program the data to go throuogh the code, not live
