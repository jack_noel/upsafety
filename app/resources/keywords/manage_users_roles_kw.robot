*** Settings ***
Documentation

Library    SeleniumLibrary
Library    String

Resource   ../variables/urls.robot
Resource    ../variables/manage_user_roles_data.robot

Variables   ../elements/ManageUsersRolesElements.py

*** Keywords ***
Navigate To User Management Role Page
	[Documentation]    Admin Is Navigating To User Management Role Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To   ${TEST_MANAGE_USERS_ROLES_URL}
	Sleep    5
	Wait Until Element Is Visible    ${AddUserRoleButton}

Create New Cloud User Role, And Asserting That New Role Exists
	${random_cloud_user_number} =    Generate Random String    6    [NUMBERS]
	${payment_clerk} =    Catenate    SEPARATOR=    Payment Clerk    ${random_cloud_user_number}
	Click Element    ${AddUserRoleButton}
	Input Text    ${RoleNameElement}    ${payment_clerk}
	Click Element    ${AddRoleSaveButtonElement}
	Element Should Be Visible    //*[text()='${payment_clerk}']


Create New Mobile User Role
	[Documentation]
	...                === END DOCUMENTATION ===
	${random_mobile_user_number} =    Generate Random String    7    [NUMBERS]
	${random_mobile_user} =    Catenate    SEPARATOR=    Mobile User    ${random_mobile_user_number}
	Click Element    ${AddUserRoleButton}
	Input Text    ${RoleNameElement}    ${random_mobile_user}
	Click Element    ${RoleTypeDropdown}
	Click Element    ${RoleTypeDropdownDevice}
	Click Element    ${AddRoleSaveButtonElement}
	Input Text    ${RoleSearchElement}    ${random_mobile_user}
	Element Should Be Visible    //*[text()='${random_mobile_user}']

Attempting To Add New Role That Already Exists
	 Click Element    ${AddUserRoleButton}
	Input Text    ${RoleNameElement}   ${test_role_name_administrator}
	Click Element    ${AddRoleSaveButtonElement}

Asserting Correct Error Message Is Displayed For User Role Duplication
	Element Should Be Visible    ${RoleAlreadyExistsErrorMessageElement}

Adding A New Permission To The Admin Role
	Click Element    //*[text()='Administrator']
	Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateIMeterInterface
	Press Keys    None    ENTER
	Click Element    //*[text()='ActivateiMeterInterface']//ancestor::tr//following::td[1]//input
	Click Element    //*[@id="role-permission-save-btn"]
	Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateIMeterInterface
	Press Keys    None    ENTER

Asserting New Permission Checkbox Is Displayed Correctly
	Element Text Should Be    //*[text()='ActivateiMeterInterface']//ancestor::tr//following::td[3]    Yes

Post Test Clean Up For New Permissions Role To The Admin
	Click Element    //*[text()='ActivateiMeterInterface']//ancestor::tr//following::td[1]//input
	Click Element    //*[@id="role-permission-save-btn"]
	Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    ActivateIMeterInterface
	Press Keys    None    ENTER
	Element Text Should Be    //*[text()='ActivateiMeterInterface']//ancestor::tr//following::td[3]    No

Assert New Permission Checkbox Is Displayed For Mobile User Correctly
	Element Text Should Be    //*[text()='Issue']//ancestor::tr//following::td[3]    Yes

Post Test Clean Up For New Permissions For Mobile User
	Click Element    //*[text()='Issue']//ancestor::tr//following::td[1]//input
	Click Element    //*[@id="role-permission-save-btn"]
	Input Text    //*[@id="gs_role-permission-grid_PermissionName"]    Issue
	Press Keys    None    ENTER
	Element Text Should Be    //*[text()='Issue']//ancestor::tr//following::td[3]    No