*** Settings ***
Documentation    This file will house all of the enter paper ticket data

Library    SeleniumLibrary
Library    String

Resource    ../variables/enter_paper_ticket_data.robot
Resource    ../variables/urls.robot
Resource    global_kw.robot

Variables    ../elements/EnterPaperTicketElements.py
Variables    ../elements/ProcessTicketsElements.py
Variables    ../elements/GlobalElements.py

*** Keywords ***
Navigate To Paper Ticket Page
	[Documentation]    Admin Is Navigating To Paper Ticket Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_ENTER_PAPER_TICKET_URL}
	Wait Until Element Is Visible    ${AcceptPaperDetailsButton}

Input Required Data For Paper Ticket Submission
	[Documentation]    Admin is creating a paper ticket that requires a number of different textfields to be populated. The textfields that are
	...                Required are as follows:
	...                Ticket Number, Officer Value, Officer Name, Badge Number, Warning Radio Button, License Number, State, Street Address
	...                Make, Type, Color Of Vehicle, Code Violation, Notes.
	...                === END DOCUMENTATION ===
	${random_ticket_number} =    Generate Random String    7    [NUMBERS]
	${random_indiana_ticket} =    Catenate    ${test_ticket_number}    ${random_ticket_number}
	${random_license_number} =    Generate Random String    8   [NUMBERS][LETTERS]
	Input Text    ${TicketNumberTextbox}   ${random_indiana_ticket}
	Click Element    ${OfficerDropdownDefaultValue}
	Click Element    ${OfficerDropdownOtherValue}
	Wait Until Element Is Visible    ${OfficerDropdownOtherNameTextbox}
	Input Text    ${OfficerDropdownOtherNameTextbox}   ${test_officer_name}
	Input Text    ${OfficerDropdownOtherBadgeNumberTextbox}   ${test_officer_badge_number}
	#Press Keys    //*[@id='IssuedDate']    TAB
	#Clear Element Text    ${IssuedDateTextbox}
	#Input Text    ${IssuedDateTextbox}   ${test_date_issued}
	Click Element    ${TicketWarningYesRadioButton}
	Input Text    ${LicenseNumberTextbox}   ${random_license_number}
	Click Element    ${StateDropdownDefaultValue}
	Click Element    ${TestStateDropdownIndiana}
	Click Element    ${StreetAddressDropdownDefaultValue}
	Click Element    ${StreetAddressDropdownOthersValue}
	Wait Until Element Is Visible    ${StreetAddressDropdownOthersValue}
	Input Text    ${OtherTextbox}   ${test_street_address}
	Click Element    ${VehicleMakeDefaultValue}
	Click Element    ${TestVehicleSubaru}
	Click Element    ${TypeDefaultValue}
	Click Element    ${TestVehicleType}
	Click Element    ${ColorDefaultValue}
	Click Element    ${TestVehicleColor}
	Click Element    ${ViolationDefaultValue}
	Click Element    ${TestCodeViolationHundredDollarFine}
	Click Element    ${NotesDefaultValue}
	Click Element    ${TestOffenceValue}
	Click Button    ${AcceptPaperDetailsButton}

Inputting Not A Warning Ticket Submission
	[Documentation]    Admin is creating a paper ticket that requires a number of different textfields to be populated. The textfields that are
	...                Required are as follows:
	...                Ticket Number, Officer Value, Officer Name, Badge Number, Warning Radio Button, License Number, State, Street Address
	...                Make, Type, Color Of Vehicle, Code Violation, Notes.
	...                All Paper Ticket Entries That Require Payment Will Be 10 Digit NUMBERS Only
	...                === END DOCUMENTATION ===
	${random_ticket_number} =    Generate Random String    10    [NUMBERS]
	${random_indiana_ticket} =    Catenate    ${test_ticket_number}    ${random_ticket_number}
	${random_license_number} =    Generate Random String    8   [NUMBERS][LETTERS]
	Input Text    ${TicketNumberTextbox}   ${random_indiana_ticket}
	Click Element    ${OfficerDropdownDefaultValue}
	Click Element    ${OfficerDropdownOtherValue}
	Input Text    ${OfficerDropdownOtherNameTextbox}   ${test_officer_name}
	Input Text    ${OfficerDropdownOtherBadgeNumberTextbox}   ${test_officer_badge_number}
	Click Element    ${TicketWarningNoRadioButton}
	Input Text    ${LicenseNumberTextbox}   ${random_license_number}
	Click Element    ${StateDropdownDefaultValue}
	Click Element    ${TestStateDropdownIndiana}
	Click Element    ${StreetAddressDropdownDefaultValue}
	Click Element    ${StreetAddressDropdownOthersValue}
	Wait Until Element Is Visible    ${StreetAddressDropdownOthersValue}
	Input Text    ${OtherTextbox}   ${test_street_address}
	Click Element    ${VehicleMakeDefaultValue}
	Click Element    ${TestVehicleSubaru}
	Click Element    ${TypeDefaultValue}
	Click Element    ${TestVehicleType}
	Click Element    ${ColorDefaultValue}
	Click Element    ${TestVehicleColor}
	Click Element    ${ViolationDefaultValue}
	Click Element    ${TestCodeViolationHundredDollarFine}
	Click Element    ${NotesDefaultValue}
	Click Element    ${TestOffenceValue}
	Click Button    ${AcceptPaperDetailsButton}

Input Test Data For Processing Ticket Tests
	[Documentation]    Admin is creating a paper ticket that requires a number of different textfields to be populated. The textfields that are
	...                Required are as follows:
	...                Ticket Number, Officer Value, Officer Name, Badge Number, Warning Radio Button, License Number, State, Street Address
	...                Make, Type, Color Of Vehicle, Code Violation, Notes.
	...                All Paper Ticket Entries That Require Payment Will Be 10 Digit NUMBERS Only
	...               *NOTE* This Method Is To Be Used For The Test Of Modifying An Already Created Paper Ticket.
	...                === END DOCUMENTATION ===

Ensure New Paper Ticket Is Visible After Creation
	[Documentation]     Asserting that the Element is visible after Paper Ticket Creation.
	...                === END DOCUMENTATION ===
	Go To    ${TEST_PROCESS_TICKETS_URL}
	Wait Until Element Is Visible    ${SearchATicketButton}
	Click Element    ${SearchATicketButton}
    Confirmation Warning Popup
	Click Element                ${PopupOkButton}
	Wait Until Element Is Visible    ${SeventyFiveDollarFineElement}
	Element Should Be Visible    ${SeventyFiveDollarFineElement}
