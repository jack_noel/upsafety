*** Settings ***
Documentation    Methods Live Here

Library     SeleniumLibrary

Resource    ../variables/setup_location_data.robot
Resource    ../variables/urls.robot

Variables   ../elements/LocationElements.py

*** Keywords ***
Navigate To Location Page
	[Documentation]    Admin Is Navigating To The Location Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_LOCATIONS_URL}
	Wait Until Element Is Visible    ${AddLocationButton}

Click Add Location Button
	[Documentation]    Admin Clicking Add Location Button
	...                === END DOCUMENTATION ===
	Click Button    ${AddLocationButton}

Click On State Dropdown
	[Documentation]    Adming Click On Indiana State Dropdown Option
	...                === END DOCUMENTATION ===
	Click Element    ${DefaultStateDropdown}
	Click Element    ${IndianaStateOption}

Click On City Dropdown
	[Documentation]    Admin Selecting Indianapolis City Option
	...                === END DOCUMENTATION ===
	Click Element    ${DefaultCityDropdown}
	Click Element    ${IndianapolisCityOption}

Populate Location Code
	[Documentation]    Admin Inputting Broadripple Location Code BR
	...                === END DOCUMENTATION ===
	Input Text    ${LocationCode}    ${code_broadripple}

Populate Location Name
	[Documentation]    Admin Inputting Broadripple
	...                === END DOCUMENTATION ===
	Input Text    ${LocationName}    ${broadripple}
