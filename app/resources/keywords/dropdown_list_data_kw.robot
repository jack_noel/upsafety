*** Settings ***
Documentation    Methods Live Here

Library     SeleniumLibrary
Library    String

Resource    ../variables/urls.robot
Resource    ../variables/dropdown_list_data_data.robot

Variables   ../elements/DropdownListDataElements.py

*** Keywords ***
Navigate To Data Dropdown List Page
	[Documentation]    Admin Is Navigating To The Dropdown List Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_DROPDOWN_LIST_DATA_URL}
	Wait Until Element Is Visible    ${WelcomeTabMessage}    10

Inputting New Vehicle Information
	[Documentation]    We Are Implementing New Vehicle Information, With 4 Random Uppercase Strings For the Vehicle Code
	...                And 29 Random Numbers, Clicking Save, Reloading Page, And Asserting That The Code & Value
	${random_vehicle_code} =    Generate Random String    4    [UPPER]
	${random_vehicle_value} =   Generate Random String    29    [NUMBERS]
    Click Element    ${VehicleOption}
    Click Element    ${SubVehicleOptionAddVehicle}
    Input Text    ${KeyCode}    ${random_vehicle_code}
    Input Text    ${KeyValue}    ${random_vehicle_value}
    Click Element    ${KeySaveButton}
    Press Keys    None    ESC
    Input Text    ${VehicleSearchColumn}   ${random_vehicle_value}
	Element Text Should Be    //*[text()='${random_vehicle_code}']    ${random_vehicle_code}
	Element Text Should Be    //*[text()='${random_vehicle_value}']     ${random_vehicle_value}

Inputting New Sex Information
	[Documentation]    We Are Testing To Make Sure The Sex Option Has The Ability To Support More Than Male & Female Selections.
	...                We Are Randoming Generating Strings And Implementing Them, Then Asserting That The Generated Codes Are Displayed Correctly.
	...                === END DOCUMENTATION ===
	${random_sex_code} =    Generate Random String    2    [UPPER]
	${random_sex_value} =   Generate Random String    6    [LETTERS]
	Click Element    ${SexOption}
	Click Element    ${SubVehicleOptionAddVehicle}
	Input Text    ${KeyCode}     ${random_sex_code}
	Input Text    ${KeyValue}    ${random_sex_value}
	Click Button    ${KeySaveButton}
    Press Keys    None    ESC
	Input Text    ${VehicleSearchColumn}   ${random_sex_value}
	Element Text Should Be    //*[text()='${random_sex_code}']      ${random_sex_code}
	Element Text Should Be    //*[text()='${random_sex_value}']      ${random_sex_value}

Inputting New Vehicle Type Information
	[Documentation]    We Are Testing To Ensure A New Vehicle Type Can Be Created And Accessed Within The Table
	...                We Are Randomingly Generating The Code And Value And Will be Asserting That Those Values Are Displayed Correctly In The Table
	...                === END DOCUMENTATION ===
	${random_vehicletype_code} =    Generate Random String    2    [UPPER]
	${random_vehicletype_value} =   Generate Random String    10    [NUMBERS]
	Click Element    ${NewVehicleTypeOption}
	Click Element    ${AddNewVehicleType}
	Input Text    ${KeyCode}    ${random_vehicletype_code}
	Input Text    ${KeyValue}    ${random_vehicletype_value}
	Click Button    ${KeySaveButton}
    Press Keys    None    ESC
   	Input Text    ${VehicleSearchColumn}   ${random_vehicletype_value}
	Element Text Should Be    //*[text()='${random_vehicletype_code}']      ${random_vehicletype_code}
	Element Text Should Be    //*[text()='${random_vehicletype_value}']      ${random_vehicletype_value}

Inputting New Color Information
	[Documentation]    We Are Testing To Ensure A New Color Type Can Be Created And Accessed Within The Table
	...                We Are Randomingly Generating The Code And Value And Will be Asserting That Those Values Are Displayed Correctly In The Table
	...                === END DOCUMENTATION ===
	${random_color_code} =    Generate Random String    2    [UPPER]
	${random_color_value} =   Generate Random String    6    [LETTERS]
	Click Element    ${ColorNCICOption}
	Click Element    ${AddNewVehicleType}
	Input Text    ${KeyCode}    ${random_color_code}
	Input Text    ${KeyValue}    ${random_color_value}
	Click Button    ${KeySaveButton}
	Press Keys    None    ESC
	Input Text    ${VehicleSearchColumn}    ${random_color_value}
	Element Text Should Be    //*[text()='${random_color_code}']      ${random_color_code}
	Element Text Should Be    //*[text()='${random_color_value}']      ${random_color_value}

Inputting New Notes Information
	[Documentation]    We Are Testing To Ensure A New Note Can Be Created And Accessed Within The Table
	...                We Are Randomingly Generating The Code And Value And Will be Asserting That Those Values Are Displayed Correctly In The Table
	...                === END DOCUMENTATION ===
	${random_notes_code} =    Generate Random String    5    [UPPER]
	${random_notes_value} =   Generate Random String    20    [LETTERS]
	Click Element    ${NotesOption}
	Click Element    ${AddNewVehicleType}
	Input Text    ${KeyCode}    ${random_notes_code}
	Input Text    ${KeyValue}    ${random_notes_value}
	Click Button    ${KeySaveButton}
	Press Keys    None    ESC
	Input Text    ${VehicleSearchColumn}    ${random_notes_value}
	Element Text Should Be    //*[text()='${random_notes_code}']      ${random_notes_code}
	Element Text Should Be    //*[text()='${random_notes_value}']      ${random_notes_value}
