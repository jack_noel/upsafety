*** Settings ***
Documentation

Library    SeleniumLibrary

Resource   ../variables/urls.robot

Variables    ../elements/TicketReportElements.py

*** Keywords ***
Navigate To Ticket Reports Page
    Go To    ${TEST_TICKET_REPORTS_URL}
    Wait Until Element Is Enabled    //*[@id="runReportBtn"]
