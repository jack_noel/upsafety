*** Settings ***
Documentation

Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../variables/import_ticket_data_data.robot
Resource    ../variables/urls.robot

Variables    ../elements/ImportTicketDataElements.py

*** Keywords ***
Navigate To Import Ticket Data Page
	[Documentation]    Admin Is Navigating To Import Ticket Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To   ${TEST_IMPORT_TICKET_DATA_URL}
	Wait Until Element Is Visible    ${DownloadTemplateButtonElement}

Click Download Ticket Master Button
	[Documentation]    Admin Is Clicking On The Master Ticket Download Button, And Asserting That The File Exists On The Hard Drive
	...                === END DOCUMENTATION ===
	${PATH} =    catenate    SEPARATOR=    ${test_cur_dir}    ${test_ticket_master_file}
	Click Element    ${DownloadTicketMasterButtonElement}
	Sleep    3
	File Should Exist    ${PATH}

Click Download Ticket Template Button
	[Documentation]    Admin Is Clicking On The Ticket Template Download Button, And Asserting That The File Exists On The Hard Drive
	...                === END DOCUMENTATION ===
	${PATH} =    catenate    SEPARATOR=    ${test_cur_dir}    ${test_ticket_template_file}
	Click Element    ${DownloadTemplateButtonElement}
	File Should Exist    ${PATH}

Uploading File To Import Data Table
	[Documentation]    Admin Is Clicking On Upload, Choose File; Button. Adming To Upload Excel File.
	...                === END DOCUMENTATION ===
	${PATH} =    catenate    SEPARATOR=    ${test_upload_file_dir}    ${test_upload_file_name}
	Click Element    ${ChooseFileUploadButtonElement}
	Choose File    ${ChooseFileUploadButtonElement}    ${PATH}

# ----- POST TEST -----
Import Ticket Master Data Cleanup
	[Documentation]    Post Clean Up, Removing Ticket Master File From Hard Drive.
	...                === END DOCUMENTATION ===
	${PATH} =    catenate    SEPARATOR=    ${test_cur_dir}    ${test_ticket_master_file}
	Remove File    ${PATH}
	File Should Not Exist    ${PATH}

Ticket Import Template Data Cleanup
	[Documentation]    Post Clean Up, Removing Ticket Template File From Hard Drive.
	...                === END DOCUMENTATION ===
	${PATH} =    catenate    SEPARATOR=    ${test_cur_dir}    ${test_ticket_template_file}
	Remove File    ${PATH}
	File Should Not Exist    ${PATH}