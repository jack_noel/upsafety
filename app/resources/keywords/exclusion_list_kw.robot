*** Settings ***
Documentation    Methods Live Here

Library     SeleniumLibrary

Resource    ../variables/exclusion_list_data.robot
Resource    ../variables/urls.robot

Variables    ../elements/ExclusionListElements.py

*** Keywords ***
Navigate To Exclusion List Page
	[Documentation]    Admin Is Navigating To The Exclusion List Page
	...                We Are Waiting For A Visible Element Before Continuing
	...                === END DOCUMENTATION ===
	Go To    ${TEST_EXCLUSION_LIST_URL}
	Wait Until Element Is Visible    ${AddExclusionButton}

Input New Exclusion Data
	[Documentation]    Admin Is Inputting All Necessary Data, On Authority Of, Description, License Number; And Clicking The Save Button
	...                === END DOCUMENTATION ===
	Click Element    ${AddExclusionButton}
	Input Text      ${OnAuthorityOfTextbox}   ${test_on_authority_of}
	Input Text      ${DescriptionTextbox}   ${test_description_name}
	Click Element    ${LicenseStateDropdownDefaultValue}
	Click Element    ${LicenseStateDropdownIndianaValue}
	Input Text     ${LicenseNumberTextbox}     ${test_license_number}
	Click Button     ${NewExclusionSaveButton}

Assert New Exclusion Data Exists
	[Documentation]    Admin Asserting That Exclusion Data Exists
	...                === END DOCUMENTATION ===
	Reload Page
	Wait Until Element Is Visible       ${NewExclusionValidationElement}
	Element Should Be Visible    ${NewExclusionValidationElement}

Post Test Cleanup
	[Documentation]    System Cleanup, Clicking The Delete Button Within The Table On The New Exclusion Data Test
	...                === END DOCUMENTATION ===
	##Execute Javascript      window.document.getElementsByClassName('material-icons md-18')[1].scrollIntoView()
	Click Element    ${TestDeleteIcon}
	Current Frame Should Contain    Please Confirm
	Element Should Be Visible       //*[@id="popup_content"]
	Element Should Be Visible        //*[@id="popup_ok"]
	Click Element                   //*[@id="popup_ok"]
	Element Should Not Be Visible       ${NewExclusionValidationElement}