*** Settings ***
Documentation

Library      SeleniumLibrary
Library      String

Resource    ../variables/violations_data.robot
Resource    ../variables/urls.robot

Variables   ../elements/ViolationsElements.py

*** Keywords ***

Navigate To Violations Page
	Go To   ${TEST_VIOLATIONS_URL}

Input Required Data For New Violation
	[Documentation]    Admin Inputting All Required Information Into Textfields For New Violating; In This Case It's
	...                Violation Code, Violation Cost, Description, Print Description
	...                === END DOCUMENTATION ===
	Input Text    ${CodeOrNumberTextbox}   ${test_new_violation_code}
	Input Text    ${ViolationCostTextbox}   ${test_violation_cost}
	Input Text    ${ScreenDescriptionTextbox}   ${test_screen_description}
	Input Text    ${PrintDescriptionTextbox}   ${test_print_description}
	Input Text    ${NumberOfImagesRequiredTextbox}   ${test_number_of_required_images}
	Click Element    ${NewViolationCodeSaveButton}

Input Required Data To Throw Duplication Error
	[Documentation]    Admin Inputting Required Data To Throw Duplication Error
	...                === END DOCUMENTATION ===
	Input Text    ${CodeOrNumberTextbox}   ${test_new_violation_code}
	Input Text    ${ViolationCostTextbox}   ${test_violation_cost}

Input Required Data For New Meter Violation
	[Documentation]    Admin Inputting Required Data For New METER Violation. In This Case It's
	...                Violation Code, Violation Cost, Description, Print Description
	...                *NOTE* The Assertion Will Be Applied At The Method Level Due To The Randomly Generated String Value
	...                === END DOCUMENTATION ===
	${random_violation_code} =    Generate Random String    15   [UPPER][NUMBERS]
	Input Text    ${CodeOrNumberTextbox}   ${random_violation_code}
	Input Text    ${ViolationCostTextbox}   ${test_meter_violation_cost}
	Input Text    ${ScreenDescriptionTextbox}   ${test_meter_screen_description}
	Input Text    ${PrintDescriptionTextbox}    ${test_meter_print_description}
	Input Text    ${NumberOfImagesRequiredTextbox}  ${test_meter_number_of_required_images}
	Click Element    ${NewViolationCodeSaveButton}
	Reload Page
	Wait Until Element Is Visible    //*[@id="globalPaging"]
	Click Element    //*[@id="globalPaging"]/option[4]
	Scroll Element Into View    //*[text()='${random_violation_code}']
	Wait Until Element Is Visible    //*[text()='${random_violation_code}']
	Element Should Be Visible    //*[text()='${random_violation_code}']

Inputting Required Data For New Non Meter Violation
	[Documentation]    Admin Inputting Required Data For New METER Violation. In This Case It's
	...                Violation Code, Violation Cost, Description, Print Description
	...                *NOTE* The Assertion Will Be Applied At The Method Level Due To The Randomly Generated String Value
	...                === END DOCUMENTATION ===
	${random_violation_code} =    Generate Random String    15   [UPPER][NUMBERS]
	Input Text    ${CodeOrNumberTextbox}   ${random_violation_code}
	Input Text    ${ViolationCostTextbox}   ${test_meter_violation_cost}
	Input Text    ${ScreenDescriptionTextbox}   ${test_meter_screen_description}
	Input Text    ${PrintDescriptionTextbox}    ${test_meter_print_description}
	Input Text    ${NumberOfImagesRequiredTextbox}  ${test_meter_number_of_required_images}
	Click Element    ${NewViolationCodeSaveButton}
	Reload Page
	Wait Until Element Is Visible    //*[@id="globalPaging"]
	Click Element    //*[@id="globalPaging"]/option[4]
	Scroll Element Into View    //*[text()='${random_violation_code}']
	Wait Until Element Is Visible    //*[text()='${random_violation_code}']
	Element Should Be Visible    //*[text()='${random_violation_code}']

Adding Violation
	Wait Until Element Is Visible    ${AddViolationButton}
	Click Element    ${AddViolationButton}

Asserting Duplicated Error Message Is Displayed
	Element Should Contain    ${DuplicatedViolationErrorMessage}    ${test_violation_already_exists_error_message}

